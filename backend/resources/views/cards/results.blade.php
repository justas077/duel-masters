@extends('layouts.mainLayout')

@section('content')
    @foreach($twitterData as $key => $value)
        <div class="well">
            @if(count(explode("https://",$value['text'])) > 1)
            {{explode("https://",$value['text'])[0]}} </br>
            
            <a href="https://{{explode("https://",$value['text'])[1]}}">
                https://{{explode("https://",$value['text'])[1]}}
            </a>
            @else
                {{$value['text']}}
            @endif
        </div>
    @endforeach
    <a href="/"><div class="btn btn-primary">Back</div></a>
@endsection
