@extends('layouts.mainLayout')

@section('content')

    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Image</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($cards as $card)
            @php
                $color = 'bg-active';
                switch ($card->civilization) {
                    case "Water":
                        $color = 'bg-info';
                        break;
                    case "Fire":
                        $color = "bg-danger";
                        break;
                    case "Light":
                        $color = 'bg-warning';
                        break;
                    case "Nature":
                        $color = 'bg-success';
                        break;
                    case "Darkness":
                        $color = 'bg-dark';
                        break;
                    default:
                        $color = 'bg-active';
                        break;
                }
            @endphp
            <tr class="{{ $color }}">
                <th scope="row">{{ $card->id }}</th>
                <td>{{ $card->name }}</td>
                <td>
                    @if($card->image != null)
                        <img src="http://duelmasters.game/cards/preview/{{$card->id}}" width="100px"/>
                    @endif
                </td>
                <td>
                    {!! Form::open(['action' => ['CardsCreationController@uploadImageFromUrl', $card->id], 'method' => 'post']) !!}
                        <div class="form-group">
                            {{Form::text('image', '', ['class' => 'form-control', 'placeholder' => 'Image URL'])}}
                        </div>
                        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $cards->links() }}

@endsection
