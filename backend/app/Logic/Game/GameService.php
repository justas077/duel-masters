<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2/2/2019
 * Time: 11:43 AM
 */

namespace App\Logic\Game;

use App\Events\DuelAction;
use App\Game;
use App\InGameCard;
use App\Logic\Game\Effects\EffectsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameService
{
    public function moveCard(InGameCard $card, $from, $to, $witch, $game = null, $report = true) : bool
    {
        $owner = $witch == "opponent" ? "me" : "opponent";
        $game = $game ?? Game::find(auth()->user()->game_id);
        $isFrom = "is_in_".$from;
        $isTo = "is_in_".$to;

        if($card->{$isFrom} == true) {
            $update = [];
            if($from == "hand" && $to == "mana") {
                if($game->{$witch}->is_added_to_mana)
                    return false;
                $game->me()->update(['is_added_to_mana' => true]);
            }

            if($to == "field")
                $update["is_summon"] = true;

            $update[$isFrom] = false;
            $update[$isTo] = true;

            $card->update($update);
            $increment = 'cards_in_'.$to;
            $decrement = 'cards_in_'.$from;
            $game->{$witch}->update([
                "{$increment}" => DB::raw($increment.'+1'),
                "{$decrement}" => DB::raw($decrement.'-1'),
            ]);
            //Report opponent about changes
            if($report)
                event(new DuelAction("MOVE", ["from" => $from, "to" => $to, "card" => $card], $owner));
            return true;
        }
        return false;
    }

    public function rotateMana($mana) : bool
    {
        $cards = collect($mana)->pluck('is_tapped', 'id')->toArray();
        $cardIds = array_keys($cards);
        $count = InGameCard::query()->whereIn('id', $cardIds)->where('created_by', auth()->user()->id)->count();

        if(count($cards) == $count) {
            $update = [];
            foreach ($cards as $card => $is_used) {
                $is_used = (int)$is_used;
                $update[] = "when id = {$card} then {$is_used}";
            }
            $whenThen = "CASE ".implode(' ', $update)." END";
            $ids = implode(',', $cardIds);
            $id = auth()->user()->id;
            DB::select("UPDATE in_game_cards SET is_tapped = ({$whenThen}) WHERE id IN ({$ids}) AND created_by = {$id}");
            event(new DuelAction("MANA-ROTATE", ["rotate" => $cards], "me"));
            return true;
        }

        return false;
    }

    public function summon(InGameCard $card, Game $game, Request $request)
    {
        $manaSummon = $game->me->in_mana_summon;
        $cost = $card->details->cost;
        $civilization = $card->details->civilization;

        if ($card->created_by == auth()->user()->id &&
            $manaSummon["Total"] >= $cost &&
            $manaSummon[$civilization] >= 1) {
            if($card->details->effect != null && $card->details->effect->activation_time == "summon")
                $this->effect($card, $request, $game);
            InGameCard::query()->where('created_by', auth()->user()->id)->where('is_tapped', true)
                ->update(['is_used_for_summon' => true]);
            $this->moveCard($card, "hand", "field", 'me', $game);
        }
    }

    private function effect(InGameCard $card, Request $request, Game $game)
    {
        $parameters = [];
        if($card->details->effect->select != null) {
            $request->validate(["selected" => "required|array|max:".$card->details->effect->amount]);
            $parameters = $request->input('selected');
            if(!$this->checkSelection($card, $parameters, $game))
                exit("wrong selection");
        }

        //CALL EFFECT FUNCTION
        $effectsService = new EffectsService($card, $parameters, $game, $this);
    }

    private function checkSelection(InGameCard $card, $selected, Game $game) : bool
    {
        $amount = $card->details->effect->amount;
        $selectAmount = count($selected);
        $isUpTo = $card->details->effect->is_up_to;
        if(!$isUpTo && $amount != $selectAmount)
            return false;
        $gameCards = InGameCard::query()->where('game_id', $game->id);
        if($card->details->effect->select_own_included) {
            $cards = $gameCards->where(function($query) use($game) {
                $query->where('created_by', $game->opponentId())->orWhere('created_by', auth()->user()->id);
            });
        } else
            $cards = $gameCards->where('created_by', $game->opponentId());

        $cards = $cards->where('is_in_'.$card->details->effect->select, true)->pluck('id')->toArray();
        if(count(array_diff(array_keys($selected), $cards)) == 0)
            return true;

        return false;
    }

    public function attack(InGameCard $attacker, $target, Game $game)
    {
        if($target == "shield")
            $defender = InGameCard::where('created_by', $game->opponentId())->where('is_in_shield', true)->first();
        else
            $defender = InGameCard::where('created_by', $game->opponentId())->where('is_in_field', true)->where('id', $target)->first();

        if($attacker->created_by == auth()->user()->id &&
            $attacker->is_in_field &&
            !$attacker->is_summon &&
            !$attacker->is_tapped &&
            $defender != null) {

            if($target == "shield") {
                $attacker->update(["is_tapped" => true]);
                event(new DuelAction("ROTATE", ["rotate" => $attacker->id, "zone" => "field"], "opponent"));
                return $this->brakeShield($defender, $game);
            } else {
                $battle = $this->battle($attacker, $defender);
                if($battle == "win") {
                    event(new DuelAction("ROTATE", ["rotate" => $attacker->id, "zone" => "field"], "opponent"));
                    $this->moveCard($defender, "field", "graveyard", "opponent", $game);
                    $attacker->update(["is_tapped" => true]);
                } else if ($battle == "lose") {
                    $this->moveCard($attacker, "field", "graveyard", "me", $game);
                } else {
                    $this->moveCard($defender, "field", "graveyard", "opponent", $game);
                    $this->moveCard($attacker, "field", "graveyard", "me", $game);
                }
                return $battle;
            }
        } else
            return null;
    }

    private function brakeShield(InGameCard $shield, Game $game)
    {
        $this->moveCard($shield, "shield", "hand", "opponent", $game);
        return "shield_braked";
    }

    private function battle(InGameCard $attacker, InGameCard $target) : string
    {
        $attack = $attacker->details->power;
        $defend = $target->details->power;

        if($attack > $defend)
            return "win";
        else if($defend > $attack)
            return "lose";
        else
            return "draw";
    }

    public function endTurn(Game $game)
    {
        if($game->me->is_my_turn) {
            $game->update([
                "turn" => DB::raw('turn+1'),
            ]);
            $game->me->update([
                "is_added_to_mana" => false,
                "is_my_turn" => false
            ]);
            $game->opponent->update([
                "is_my_turn" => true
            ]);

            InGameCard::query()->where('game_id', $game->id)->where('created_by', $game->opponentId())
                ->where(function($query) {
                    $query->where('is_tapped', true)->orWhere(function($query) {
                        $query->orWhere('is_used_for_summon', true)->orWhere('is_summon', true);
                    });
                })->update([
                    'is_tapped' => false,
                    'is_used_for_summon' => false,
                    'is_summon' => false
                ]);

            $drawCard = InGameCard::where('game_id', $game->id)
                ->where('created_by', $game->opponentId())->where('is_in_deck', true)->first();

            $this->moveCard($drawCard, "deck", "hand", 'opponent', $game, false);

            event(new DuelAction("START-TURN", []));
        }
    }
}