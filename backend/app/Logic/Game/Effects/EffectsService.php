<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2/17/2019
 * Time: 12:11 PM
 */

namespace App\Logic\Game\Effects;


use App\Game;
use App\InGameCard;
use App\Logic\Game\GameService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EffectsService
{
    private $actions;
    private $card;
    private $parameters;
    private $game;
    private $move;

    public $response = null;

    public function __construct(InGameCard $card, $parameters, Game $game, GameService $move)
    {
        $this->actions = new EffectsActions();
        $this->card = $card;
        $this->parameters = $parameters;
        $this->game = $game;
        $this->move = $move;

        $this->response = $this->{$card->details->effect->function_name}($card->details->effect->select);
    }

    private function return_to_hand($select)
    {
        foreach ($this->parameters as $card => $owner) {
            $this->move->moveCard(InGameCard::findOrFail($card), $select, "hand", $owner, $this->game);
        }

    }
}