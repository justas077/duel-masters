<?php
namespace App\Logic\Game;


use App\Deck;
use App\DeckCard;
use App\Game;
use App\GamePlayer;
use App\InGameCard;
use App\User;
use Carbon\Carbon;

class CreateService
{
    public function gamePlayers(Game $game)
    {
        $now = Carbon::now();
        $insertData = [
            [
                "game_id" => $game->id,
                "created_by" => $game->first_player,
                "updated_by" => $game->first_player,
                "is_my_turn" => true,
                "created_at" => $now,
                "updated_at" => $now
            ],
            [
                "game_id" => $game->id,
                "created_by" => $game->second_player,
                "updated_by" => $game->second_player,
                "is_my_turn" => false,
                "created_at" => $now,
                "updated_at" => $now
            ]
        ];
        GamePlayer::insert($insertData);
    }

    public function inGameCards(Game $game)
    {
        $this->prepareDeckForGame($game->first_player, $game->id);
        $this->prepareDeckForGame($game->second_player, $game->id);
    }

    private function prepareDeckForGame($userId, $gameId)
    {
        $deckId = Deck::query()->where('created_by', $userId)->where('is_active', true)->first()->id;
        $deck = DeckCard::query()->where('deck_id', $deckId)->pluck('amount', 'card_id');
        $deckCards = [];
        foreach ($deck as $card => $amount) {
            if($amount > 1)
                $deckCards = array_pad($deckCards, count($deckCards) + $amount, $card);
            else
                $deckCards[] = $card;
        }
        shuffle($deckCards);
        $hand = array_splice($deckCards, 0, 5);
        $shields = array_splice($deckCards, 0, 5);
        $insertData = [];
        foreach ($hand as $card) {
            $insertData[] = [
                "card_id" => $card,
                "game_id" => $gameId,
                "is_in_hand" => true,
                "is_in_shield" => false,
                "is_in_deck" => false,
                "created_by" => $userId,
                "updated_by" => $userId
            ];
        }

        foreach ($shields as $card) {
            $insertData[] = [
                "card_id" => $card,
                "game_id" => $gameId,
                "is_in_hand" => false,
                "is_in_shield" => true,
                "is_in_deck" => false,
                "created_by" => $userId,
                "updated_by" => $userId
            ];
        }

        foreach ($deckCards as $card) {
            $insertData[] = [
                "card_id" => $card,
                "game_id" => $gameId,
                "is_in_hand" => false,
                "is_in_shield" => false,
                "is_in_deck" => true,
                "created_by" => $userId,
                "updated_by" => $userId
            ];
        }

        InGameCard::insert($insertData);
        GamePlayer::query()->where('created_by', $userId)->where('game_id', $gameId)->first()->update([
            "cards_in_deck" => count($deckCards)
        ]);
    }
}