<?php
/**
 * Created by PhpStorm.
 * User: jusden
 * Date: 1/29/2019
 * Time: 10:33 PM
 */

namespace App\Logic\Traits;


use Illuminate\Support\Facades\Auth;

trait BootableTrait
{
    public static function bootBootableTrait()
    {
        static::creating(function($table)  {
            $table->created_by = (Auth::check())?Auth::user()->id:1;
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::updating(function($table)  {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });
    }
}
