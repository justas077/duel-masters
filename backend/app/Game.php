<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $with = [
        'me',
        'opponent'
    ];

    protected $fillable = [
        'first_player',
        'second_player',
        'status',
        'turn'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function me()
    {
        return $this->hasOne(GamePlayer::class, 'game_id', 'id')
            ->where('created_by', auth()->user()->id)
            ->with(['hand', 'mana', 'field', 'graveyard']);
    }

    public function opponent()
    {
        return $this->hasOne(GamePlayer::class, 'game_id', 'id')
            ->where('created_by', '!=', auth()->user()->id)
            ->with(['omana', 'ofield', 'ograveyard']);
    }

    public function opponentId()
    {
        return $this->first_player == auth()->user()->id ? $this->second_player : $this->first_player;
    }
}
