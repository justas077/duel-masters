<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    public $timestamps = false;

    protected $with = [
        "effect"
    ];

    protected $fillable = [
        "image"
    ];

    protected $appends = [
        'image_address'
    ];

    public function getImageAddressAttribute()
    {
        return "../../../../assets/images/cards/". rawurlencode($this->name) . ".jpg";
    }

    public function effect()
    {
        return $this->hasOne(Effect::class, 'id', 'effect_id');
    }
}
