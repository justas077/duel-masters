<?php

namespace App;

use App\Logic\Traits\BootableTrait;
use Illuminate\Database\Eloquent\Model;

class GamePlayer extends Model
{
    use BootableTrait;

    protected $fillable = [
        'game_id',
        'cards_in_hand',
        'cards_in_deck',
        'cards_in_mana',
        'cards_in_graveyard',
        'cards_in_extra_deck',
        'cards_in_field',
        'cards_in_shield',
        'is_added_to_mana',
        'is_my_turn',
        'need_select'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'created_by',
        'updated_by'
    ];

    protected $casts = [
        'is_added_to_mana' => 'boolean',
        'is_my_turn' => 'boolean',
        'need_select' => 'boolean'
    ];


    protected $appends = [
        'in_mana_summon'
    ];


    public function getInManaSummonAttribute()
    {
        $result = [
            "Water" => 0,
            "Fire" => 0,
            "Nature" => 0,
            "Light" => 0,
            "Darkness" => 0,
            "Total" => 0
        ];
        $civilizations = $this->mana()->where('is_tapped', true)->where('is_used_for_summon', false)
            ->get()->pluck('details.civilization')->toArray();
        foreach ($civilizations as $civilization) {
            $result[$civilization]++;
            $result["Total"]++;
        }

        return $result;
    }

    public function hand()
    {
        return $this->hasMany(InGameCard::class, 'game_id', 'game_id')
            ->where('created_by', auth()->user()->id)->where('is_in_hand', true);
    }

    public function mana()
    {
        return $this->hasMany(InGameCard::class, 'game_id', 'game_id')
            ->where('created_by', auth()->user()->id)->where('is_in_mana', true);
    }

    public function field()
    {
        return $this->hasMany(InGameCard::class, 'game_id', 'game_id')
            ->where('created_by', auth()->user()->id)->where('is_in_field', true);
    }

    public function graveyard()
    {
        return $this->hasMany(InGameCard::class, 'game_id', 'game_id')
            ->where('created_by', auth()->user()->id)->where('is_in_graveyard', true);
    }

    public function omana()
    {
        return $this->hasMany(InGameCard::class, 'game_id', 'game_id')
            ->where('created_by', '!=', auth()->user()->id)->where('is_in_mana', true);
    }

    public function ofield()
    {
        return $this->hasMany(InGameCard::class, 'game_id', 'game_id')
            ->where('created_by', '!=', auth()->user()->id)->where('is_in_field', true);
    }

    public function ograveyard()
    {
        return $this->hasMany(InGameCard::class, 'game_id', 'game_id')
            ->where('created_by', '!=', auth()->user()->id)->where('is_in_graveyard', true);
    }
}
