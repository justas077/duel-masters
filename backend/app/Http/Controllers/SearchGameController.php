<?php

namespace App\Http\Controllers;

use App\Events\GameSearch;
use App\Game;
use App\Logic\Game\CreateService;
use App\User;
use Illuminate\Http\Request;

class SearchGameController extends Controller
{
    public function __invoke()
    {
        $existingGame = Game::query()->where('first_player', auth()->user()->id)->orWhere('second_player', auth()->user()->id)->first();
        if($existingGame)
            return response()->json(["status" => "error", "message" => "Game already exists."], 500);
        $game = Game::query()->where('status', 'searching')->first();
        if(is_null($game)) {
            $game = Game::create(['first_player' => auth()->user()->id]);
            User::find(auth()->user()->id)->update(["game_id" => $game->id]);
            return response()->json(["status" => "success", "message" => "Session has been started.", "first_player" => true]);
        }

        $game->update(['second_player' => auth()->user()->id, 'status' => 'starting']);
        User::find(auth()->user()->id)->update(["game_id" => $game->id]);
        event(new GameSearch($game->first_player, $game->id));
        $this->start($game->fresh());
        return response()->json(["status" => "success", "message" => "Opponent found.", "first_player" => false, "game_id" => $game->id]);
    }

    private function start(Game $game)
    {
//        if($game->status != "starting")
//            return response()->json(["status" => "error", "message" => "Access denied."], 500);

        $create = new CreateService();

        $create->gamePlayers($game);
        $create->inGameCards($game);
        $game->status = "active";
        $game->save();
        //return "Decks was created.";
    }
}
