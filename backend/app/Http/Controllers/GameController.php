<?php

namespace App\Http\Controllers;

use App\Card;
use App\Deck;
use App\Events\DuelAction;
use App\Game;
use App\GamePlayer;
use App\Http\Requests\Game\ConfirmManaRequest;
use App\Http\Requests\Game\MoveCardRequest;
use App\InGameCard;
use App\Logic\Game\GameService;
use App\Logic\Game\CreateService;
use App\Logic\Game\Effects\EffectsService;
use App\PlayerCard;
use App\User;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
    private $game;
    private $service;

    public function __construct()
    {
        $this->service = new GameService();
        $this->middleware(function ($request, $next) {
            $this->game = Game::find(auth()->user()->game_id);
            return $next($request);
        });
    }

    public function checkGameStatus($status)
    {
        if($this->game->status != $status)
            return response()->json(["status" => "error", "message" => "Access denied."], 500);
    }

    //Paspaudus ieskoti zaidimo bus sukuriamas naujas game.
    //Kitas zaidejas joinins prie to game
    public function start(Game $game)
    {
        $create = new CreateService();
        $this->checkGameStatus("starting");

        $create->gamePlayers($game);
        $create->inGameCards($game);
        $game->status = "active";
        $game->save();
        return "Decks was created.";
    }

    public function index()
    {
        $game = $this->game;
        $game["opponent"]["field"] = $game["opponent"]["ofield"];
        $game["opponent"]["mana"] = $game["opponent"]["omana"];
        $game["opponent"]["graveyard"] = $game["opponent"]["ograveyard"];
        unset($game["opponent"]["ofield"]);
        unset($game["opponent"]["omana"]);
        unset($game["opponent"]["ograveyard"]);
        return $game;
    }

    public function moveCard(InGameCard $card, MoveCardRequest $request)
    {
        if($card->created_by == auth()->user()->id)
            if($this->service->moveCard($card, $request->input('from'), $request->input('to'), 'me'))
                return response()->json(["status" => "success", "message" => "Card was moved."], 200);

        return response()->json(["status" => "error", "message" => "Unknown error."], 500);
    }

    public function confirmMana(ConfirmManaRequest $request)
    {
        $mana = $request->input('mana');
        return $this->functionResponse($this->service->rotateMana($mana), "Mana has been saved.");
    }

    public function summon(InGameCard $card, Request $request)
    {
        $this->service->summon($card, $this->game, $request);
    }

    private function functionResponse($bool, $message)
    {
        if($bool) {
            return response()->json(["status" => "success", "message" => $message], 200);
        }
        return response()->json(["status" => "error", "message" => "Unknown error."], 500);
    }

//    public function effect(InGameCard $card, Request $request)
//    {
//        //check is card okay
//        $effectsService = new EffectsService($card, $request, $this->game, $this->service);
//        return $effectsService->response;
//    }

    public function attack(InGameCard $card, Request $request)
    {
        $request->validate(["target" => "required"]);
        $status = $this->service->attack($card, $request->input('target'), $this->game);
        if($status != null)
            return ["status" => "success", "message" => $status];
        else
            return response()->json(["status" => "error", "message" => "Wrong attacker or defender."], 500);
    }

    public function endTurn()
    {
        $this->service->endTurn($this->game);
    }

}
