<?php

namespace App\Http\Controllers;

use App\Card;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CardsCreationController extends Controller
{
    public function index()
    {
        //https://www.kelz0r.dk
        return view('cards.index')->with(["cards" => Card::paginate(10)]);
    }

    public function uploadImageFromUrl(Card $card, Request $request)
    {
        if(!$request->filled('image'))
            exit('No image');
        $imageUrl = $request->input('image');
        $urlArr = explode('.', $imageUrl);
        $contents = file_get_contents($imageUrl);
        $name = md5(str_random(16)).'.'.last($urlArr);
        if($card->image != null) {
            Storage::disk('cards')->delete($card->image);
        }
        $card->update(["image" => $name]);
        Storage::disk('cards')->put($name, $contents);

        return redirect(url()->previous());
    }

    public function imagePreview(Card $card)
    {
        header('Content-Type: image/jpeg');
        readfile(storage_path('app/cards/'.$card->image));
    }

    public function createFromTxt($txt_name)
    {
        $params = [
            "Card Name" => "name",
            "Civilization" => "civilization",
            "Type" => "type",
            "Race" => "race",
            "Cost" => "cost",
            "Power" => "power",
            "Flavor Text" => "flavor_text",
            "Rarity" => "rarity",
            "Artist" => "artist"
        ];

        $fileName = $txt_name.".txt";
        $filePath = base_path('../materials/cards/data-txt/'.$fileName);

        $handle = fopen($filePath, "r");
        if ($handle) {
            $count = 0;
            $cardCount = 1;
            $card = new \App\Card();
            echo "<b>{$cardCount}.</b> Sukurta korta.</br>";
            while (($line = fgets($handle)) !== false) {
                if($count >= 9) {
                    echo "Korta išsaugota.</br></br>";
                    $card->save();
                    $cardCount++;
                    $card = new \App\Card();
                    echo "Sukurta <b>{$card->id}</b> korta.</br>";
                    $count = 0;
                }

                $parameter = explode(": ", $line);
                if(count($parameter) >= 2) {
                    $parameterName = $parameter[0];
                    $parameterValue = trim(preg_replace('/\s\s+/', '', $parameter[1]));
                    if($parameterName == "Power")
                        $parameterValue = str_replace('+', '', $parameterValue);
                    if(in_array($parameterName, array_keys($params))) {
                        $card->{$params[$parameterName]} = $parameterValue;
                        echo "{$params[$parameterName]} ===> {$parameterValue}</br>";
                        $count++;
                    }
                }
            }
            $card->save();
            fclose($handle);
        } else {
            exit("file not found.");
        }
    }
}
