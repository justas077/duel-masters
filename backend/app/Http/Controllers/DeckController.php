<?php

namespace App\Http\Controllers;

use App\Deck;
use App\DeckCard;
use App\PlayerCard;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DeckController extends Controller
{
    public function index()
    {
        $deck = Deck::query()->where('created_by', auth()->user()->id)
                ->where('is_active', true)->withCount('cards')->first();

        $amounts = [];
        $inserted = 0;
        foreach ($deck->cards as $index => $card) {
            if($card->amount > 1) {
                for($i = 1; $i < $card->amount; $i++) {
                    $deck->cards->splice($index + $inserted, 0, [$card]);
                    $inserted++;
                }
            }
            $amounts[$card->card_id] = $card->amount;
        }
        $deck["amounts"] = $amounts;
        return $deck;
    }

    public function cards()
    {
        return PlayerCard::query()->where('created_by', auth()->user()->id)->get();
    }

    public function list()
    {
        return array_chunk($this->index()->cards->toArray(), 8);
    }

    public function cardsList()
    {
        return array_chunk($this->cards()->toArray(), 8);
    }

    public function save(Request $request)
    {
        $deckId = Deck::query()->where('created_by', auth()->user()->id)->where('is_active', true)->first()->id;
        $cards = $request->input('deck');
//        $cards = $this->index()->cards->toArray();
        $newCards = array_column($cards, 'amount', 'card_id');
        $playerCards = PlayerCard::query()->where('created_by', auth()->user()->id)->pluck('amount', 'card_id')->toArray();
        DeckCard::query()->where('deck_id', $deckId)->delete();
        $insert = [];
        foreach ($newCards as $card => $amount) {
            if (array_key_exists($card, $playerCards) && $playerCards[$card] >= $amount && $amount <= 4) {
                $insert[] = [
                    "deck_id" => $deckId,
                    "card_id" => $card,
                    "amount" => $amount,
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now()
                ];
            } else {
                return response()->json(["status" => "error", "message" => "Error while creating new cards."], 500);
            }
        }

        DeckCard::insert($insert);
        return response()->json(["status" => "success", "message" => "Deck Was saved."], 200);
    }
}
