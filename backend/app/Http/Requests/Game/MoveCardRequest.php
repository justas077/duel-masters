<?php

namespace App\Http\Requests\Game;

use App\InGameCard;
use Illuminate\Foundation\Http\FormRequest;

class MoveCardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from' => 'required|in:'.implode(',', InGameCard::AVAILABLE_IN),
            'to' => 'required|in:'.implode(',', InGameCard::AVAILABLE_IN)
        ];
    }
}
