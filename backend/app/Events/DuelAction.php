<?php

namespace App\Events;

use App\Game;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DuelAction implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $action; // MOVE
    public $actionParameters; // FROM-TO
    public $owner;
    private $userId;

    /**
     * Create a new event instance.
     *
     * @param $action
     * @param $actionParameters
     * @param $owner
     */
    public function __construct($action, $actionParameters, $owner = null)
    {
        $authUser = auth()->user();
        $this->action = $action;
        $this->actionParameters = $actionParameters;
        $this->userId = Game::find($authUser->game_id)->opponentId();
        $this->owner = $owner;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('duel-player.'.$this->userId);
    }

    public function broadcastAs()
    {
        return 'duel-action';
    }
}
