<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeckCard extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $with = [
        'details'
    ];

    public function details()
    {
        return $this->hasOne(Card::class, 'id', 'card_id');
    }
}
