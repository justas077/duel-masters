<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayerCard extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at',
        'created_by',
        'updated_by'
    ];

    protected $with = [
        'details'
    ];

    public function details()
    {
        return $this->hasOne(Card::class, 'id', 'card_id');
    }
}
