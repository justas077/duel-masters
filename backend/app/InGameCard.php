<?php

namespace App;

use App\Logic\Traits\BootableTrait;
use Illuminate\Database\Eloquent\Model;

class InGameCard extends Model
{
    use BootableTrait;

    const AVAILABLE_IN = [
        'hand',
        'mana',
        'deck',
        'field',
        'shield',
        'graveyard',
        'extra_deck'
    ];

    protected $hidden = [
        "is_in_hand",
        "is_in_mana",
        "is_in_deck",
        "is_in_field",
        "is_in_shield",
        "is_in_graveyard",
        "is_in_extra_deck",
        "created_by",
        "updated_by",
        "created_at",
        "updated_at"
    ];

    protected $fillable = [
        "is_in_hand",
        "is_in_mana",
        "is_in_deck",
        "is_in_field",
        "is_in_shield",
        "is_in_graveyard",
        "is_in_extra_deck",
        "is_tapped",
        "is_used_for_summon",
        "is_summon"
    ];

    protected $with = [
        'details'
    ];

    protected $casts = [
        "is_tapped" => "boolean",
        "is_used_for_summon" => "boolean",
        'is_summon' => 'boolean'
    ];

    public function details()
    {
        return $this->hasOne(Card::class, 'id', 'card_id');
    }

}
