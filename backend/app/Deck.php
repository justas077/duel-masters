<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deck extends Model
{
    protected $fillable = [
        'name',
        'is_active'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'created_by',
        'updated_by'
    ];

    protected $with = [
        'cards'
    ];

    public function cards()
    {
        return $this->hasMany(DeckCard::class, 'deck_id', 'id');
    }
}
