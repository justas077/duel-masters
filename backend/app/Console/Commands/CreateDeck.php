<?php

namespace App\Console\Commands;

use App\Card;
use App\Deck;
use App\DeckCard;
use Illuminate\Console\Command;

class CreateDeck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create-deck {user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Function to fill user deck.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = $this->argument('user');
        $allCards = Card::all()->pluck('id')->toArray();
        shuffle($allCards);
        $deckCards = array_slice($allCards, 0, 40);
        $deck = new Deck;
        $deck->created_by = $user;
        $deck->updated_by = $user;
        $deck->save();
        $insertData = [];
        foreach ($deckCards as $deckCard) {
            $insertData[] = [
                "deck_id" => $deck->id,
                "card_id" => $deckCard
            ];
        }
        DeckCard::insert($insertData);

        $this->info("Success");
    }
}
