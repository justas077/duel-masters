<?php

namespace App\Console\Commands;

use App\Card;
use App\PlayerCard;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GiveAllCards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'give-all-cards {user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Function to give all existing cards to user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = $this->argument('user');
        $userCards = PlayerCard::query()->where('created_by', $user);
        $userCards->update(["amount" => 4]);
        $ignore = $userCards->where('amount', '>=', 4)->pluck('card_id')->toArray();
        $allCards = Card::query()->pluck('id')->toArray();

        $insert = [];
        $cards = array_diff($allCards, $ignore);
        foreach ($cards as $card) {
            $insert[] = [
                "card_id" => $card,
                "amount" => 4,
                "created_by" => $user,
                "updated_by" => $user,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
            ];
        }

        PlayerCard::insert($insert);
        $this->info("Success");
    }
}
