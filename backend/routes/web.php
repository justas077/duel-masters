<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return "VEIKIA";
});

Route::get('cards', 'CardsCreationController@index');
Route::get('cards/create-from-txt/{txt_name}', 'CardsCreationController@createFromTxt');
Route::get('cards/preview/{card}', 'CardsCreationController@imagePreview');
Route::post('cards/upload-image/{card}', 'CardsCreationController@uploadImageFromUrl');


//Route::get('game', 'GameController@index');