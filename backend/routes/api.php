<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {

    Route::get('user', function () {
        return \Illuminate\Support\Facades\Auth::user();
    });

    Route::prefix('game')->group(function () {
        Route::get('/search', 'SearchGameController');
        Route::get('/', 'GameController@index');
        Route::get('/start/{game}', 'GameController@start');
        Route::put('/move-card/{card}', 'GameController@moveCard');
        Route::put('/confirm-mana', 'GameController@confirmMana');
        Route::put('/summon/{card}', 'GameController@summon');
        Route::put('/effect/{card}', 'GameController@effect');
        Route::get('/end-turn', 'GameController@endTurn');
        Route::put('/attack/{card}', 'GameController@attack');
    });


    Route::get('deck', 'DeckController@index');
    Route::get('deck-list', 'DeckController@list');
    Route::get('player-cards', 'DeckController@cards');
    Route::get('player-cards-list', 'DeckController@cardsList');
    Route::put('save-deck', 'DeckController@save');

    Route::delete('delete-game/{game}', function(\App\Game $game) {
        \App\InGameCard::query()->where('game_id', $game->id)->delete();
        \App\GamePlayer::query()->where('game_id', $game->id)->delete();
        $game->delete();
    });

});
