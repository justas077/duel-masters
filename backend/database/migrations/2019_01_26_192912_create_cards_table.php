<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('image')->nullable();
            $table->string('civilization');
            $table->string('race')->nullable();
            $table->string('type');
            $table->smallInteger('cost');
            $table->integer('power')->default(0);
            $table->text('flavor_text')->nullable();
            $table->string('rarity')->nullable();
            $table->string('artist')->nullable();
            $table->unsignedInteger('effect_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
