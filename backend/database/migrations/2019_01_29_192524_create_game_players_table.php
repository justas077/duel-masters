<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_players', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('game_id');
            $table->unsignedInteger('cards_in_hand')->default(5);
            $table->unsignedInteger('cards_in_deck')->default(0);
            $table->unsignedInteger('cards_in_mana')->default(0);
            $table->unsignedInteger('cards_in_graveyard')->default(0);
            $table->unsignedInteger('cards_in_extra_deck')->default(0);
            $table->unsignedInteger('cards_in_field')->default(0);
            $table->unsignedInteger('cards_in_shield')->default(5);
            $table->boolean('is_added_to_mana')->default(false);
            $table->boolean('is_my_turn')->default(false);
            $table->boolean('need_select')->default(false);

            $table->unsignedInteger('created_by');
            $table->unsignedInteger('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_players');
    }
}
