<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInGameCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('in_game_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('card_id');
            $table->unsignedInteger('game_id');
            $table->boolean('is_in_hand')->default(false);
            $table->boolean('is_in_mana')->default(false);
            $table->boolean('is_in_deck')->default(false);
            $table->boolean('is_in_field')->default(false);
            $table->boolean('is_in_shield')->default(false);
            $table->boolean('is_in_graveyard')->default(false);
            $table->boolean('is_in_extra_deck')->default(false);
            $table->boolean('is_tapped')->default(false);
            $table->boolean('is_used_for_summon')->default(false);
            $table->boolean('is_summon')->default(false);

            $table->unsignedInteger('created_by');
            $table->unsignedInteger('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('in_game_cards');
    }
}
