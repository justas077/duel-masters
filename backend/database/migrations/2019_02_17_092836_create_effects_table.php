<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEffectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('effects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('function_name');
            $table->string('activation_time')->nullable();
            $table->string("select")->nullable();
            $table->string("add_from")->nullable();
            $table->string("add_to")->nullable();
            $table->boolean("select_own_included")->default(false);
            $table->boolean('is_response_need')->default(false);
            $table->integer('amount')->default(1);
            $table->boolean('is_up_to')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('effects');
    }
}
