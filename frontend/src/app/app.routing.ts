import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './pages/main/login/login.component';
import { ArenaComponent } from './pages/duel/arena/arena.component';
import { AuthGuard } from './_shared/guards/auth.guard';
import { GameComponent } from './pages/game/game.component';
import { MainComponent } from './pages/main/main.component';
import { DeckComponent } from './pages/game/deck/deck.component';

const routes: Routes = [
    { path: '',                   component: MainComponent, children: [
        { path: 'login',              component: LoginComponent },
    ] },
    
    { path: 'game',               component: GameComponent, children: [
        { path: 'deck',               component: DeckComponent }
    ], canActivate: [AuthGuard] },

    { path: 'game/arena',         component: ArenaComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
