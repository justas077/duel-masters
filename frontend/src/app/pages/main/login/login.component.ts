import { Component, OnInit } from '@angular/core';
import { RequestsService } from '../../../_services/requests.service';
import { TokenResponse } from '../../../_shared/interfaces/token-response-interface';
import { HttpErrorResponse } from '@angular/common/http';
import { User } from '../../../_shared/models/user.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/_services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../main.component.css']
})
export class LoginComponent implements OnInit {
    signInForm: FormGroup;
    isLoading = false;
    error = null;

    constructor(private requests: RequestsService, private router: Router, private authService: AuthService) {
        if (this.authService.loggedIn()) {
            this.router.navigate(['game']);
        }
    }

    ngOnInit()
    {
        this.signInForm = new FormGroup({
            'username': new FormControl(null, [Validators.required, Validators.email]),
            'password': new FormControl(null, Validators.required)
        });
    }

    onSubmit()
    {
        this.isLoading = true;
        this.error = null;
        this.requests.getToken(this.signInForm.value["username"], this.signInForm.value["password"]).subscribe(
            (data: TokenResponse) => {
                localStorage.setItem('access_token', data.access_token); 
                this.requests.getCurrentUser().subscribe(
                    (data: User) => {
                        localStorage.setItem('current_user', data.id);
                        this.isLoading = false;
                        this.router.navigate(['game']);
                    }
                );
            },
            (error: HttpErrorResponse) => {
                this.error = error.message; 
                this.isLoading = false;
            }
        );
    }

    closeError()
    {
        this.error = null;
    }

    isValid()
    {
        return  this.signInForm.get('username').valid &&
                this.signInForm.get('username').touched &&
                this.signInForm.get('password').valid &&
                !this.isLoading && this.error == null; 
    }
}
