declare const Pusher: any;
import { Component, OnInit } from '@angular/core';
import { GameService } from '../../../_services/game.service';
import { PusherService } from 'src/app/_services/pusher.service';
import { InGameCard } from 'src/app/_shared/models/in-game-card.model';
import { GameAction } from 'src/app/_shared/models/game-action.model';
import { GameActionService } from 'src/app/_services/game.action.service';
import { PlayerCard } from '../../../_shared/models/player-card.model';
import { EffectsService } from '../../../_services/effects.service';

@Component({
  selector: 'app-arena',
  templateUrl: './arena.component.html',
  styleUrls: ['./arena.component.css', '../../game/game.component.css']
})
export class ArenaComponent implements OnInit {
  readyToUseMana = false;
  readyToAttack = false;

  gameActionService: GameActionService;
  
  activeCard: PlayerCard = null;

  selectedObj: Object = new Object();
  selectZones: Array<string> = [];
  effectCard: InGameCard = null;

  constructor(private gameService: GameService, private pusherService: PusherService, private effectsService: EffectsService) {
    this.gameService.setGame();
  }

  ngOnInit() {
    this.gameActionService = new GameActionService(this.pusherService, this.gameService)
  }

  toggleUseMana()
  {
    this.readyToUseMana = !this.readyToUseMana;
  }

  private moveCardFrontend(index: number, card: InGameCard, from: string, to: string)
  {
    this['gameService']['me'][from].splice(index, 1);
    this['gameService']['me'][to].push(card);
    this['gameService']['me']['cards_in_' + from]--;
    this['gameService']['me']['cards_in_' + to]++;
  }

  moveCard(event)
  {
    let card: InGameCard = this['gameService']['me'][event.from][event.index];
    if(event.from == "hand" && event.to == "mana") {
      if(this.gameService.me.is_added_to_mana) {
        return;
      }
      this.gameService.me.is_added_to_mana = true;
    }
    
    this.moveCardFrontend(event.index, card, event.from, event.to);
    this.gameService.moveCard(event.id, event.from, event.to);
  }

  canSummon(index: number) : boolean
  {
    let card: InGameCard = this.gameService.me.hand[index];
    
    return this.gameService.me.in_mana_summon["Total"] >= card.details.cost &&
           this.gameService.me.in_mana_summon[card.details.civilization] >= 1;
  }

  rotateMana(index)
  {
    let card = this.gameService.me.mana[index];
    let isRotated = !card.is_tapped;
    card.is_tapped = isRotated;
    if(isRotated) {
      this.gameService.me.in_mana_summon[card.details.civilization]++;
      this.gameService.me.in_mana_summon["Total"]++;
    } else {
      this.gameService.me.in_mana_summon[card.details.civilization]--;
      this.gameService.me.in_mana_summon["Total"]--;
    }
  }

  confirmMana()
  {
    this.readyToUseMana = false;
    this.gameService.confirmMana();
  }

  summon(index: number)
  {
    
    let card = this.gameService.me.hand[index];
    this.gameService.me.mana.forEach((card: InGameCard) => {
      if(card.is_tapped)
        card.is_used_for_summon = true;
    });
    this.gameService.me.in_mana_summon["Water"] = 0;
    this.gameService.me.in_mana_summon["Fire"] = 0;
    this.gameService.me.in_mana_summon["Nature"] = 0;
    this.gameService.me.in_mana_summon["Light"] = 0;
    this.gameService.me.in_mana_summon["Darkness"] = 0;
    this.gameService.me.in_mana_summon["Total"] = 0;
    this.moveCardFrontend(index, card, "hand", "field");
    card.is_summon = true;

    if(card.details.effect != null && card.details.effect.activation_time == "summon") {
      this.effectCard = card;
      this.activateEffect(card);
    } else {
      this.gameService.summon(card.id);
    }
  }

  activateEffect(card: InGameCard)
  {
    if(card.details.effect.select != null) {
      this.selectZones.push("o" + card.details.effect.select);
      if(card.details.effect.select_own_included)
        this.selectZones.push(card.details.effect.select);
      this.setSelect(this['gameService']['me'][card.details.effect.select], true);
      if(card.details.effect.select_own_included)
        this.setSelect(this['gameService']['opponent']["o"+card.details.effect.select], true);
    }
  }

  confirmSelection()
  {
    this.gameService.summon(this.effectCard.id, this.selectedObj);
    this.effectsService[this.effectCard.details.effect.function_name](this.effectCard, this.selectedObj);
    this.setSelect(this['gameService']['me'][this.effectCard.details.effect.select], false);
      if(this.effectCard.details.effect.select_own_included)
        this.setSelect(this['gameService']['opponent']["o"+this.effectCard.details.effect.select], false);
    this.effectCard = null;
  }

  setSelect(array: Array<InGameCard>, status: boolean) {
    array.forEach((card: InGameCard) => {
      card.need_select = status;
    });
  }

  endTurn()
  {
    this.gameService.endTurn();
    this.readyToAttack = false;
    this.readyToUseMana = false;
  }

  setActive(card: PlayerCard)
  {
    this.activeCard = card;
  }

  toggleSelectCard(event)
  {
    let card: InGameCard = this.gameService[event.owner][event.type][event.index];
    if(!card.selected)
      this.selectedObj[card.id] = event.owner;
    else
      delete this.selectedObj[card.id];

    card.selected = !card.selected;
  }

  startAttack()
  {
    this.readyToAttack = true;
  }

  attack(event)
  {
    let target = event.target == "shield" ? "shield" : this.gameService.opponent.field[event.target].id;
    let attack: InGameCard = this.gameService.me.field[event.attacker];
    this.gameService.attack(
      attack.id,
      target,
      event.attacker,
      event.target
    );
    attack.is_tapped = true;
  }

  zoneObject() : Object
  {
    return {
      "readyToUseMana" : this.readyToUseMana,
      "readyToAttack" : this.readyToAttack,
      "isAddedToMana" : this.gameService.me.is_added_to_mana,
      "inManaSummon" : this.gameService.me.in_mana_summon,
      "isMyTurn" : this.gameService.isMyTurn,
      "selectZones" : this.selectZones
    };
  }
}
