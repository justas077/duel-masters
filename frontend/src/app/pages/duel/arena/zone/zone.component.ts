import { Component, OnInit, Input, Output } from '@angular/core';
import { InGameCard } from '../../../../_shared/models/in-game-card.model';
import { EventEmitter } from '@angular/core';
import { PlayerCard } from 'src/app/_shared/models/player-card.model';
import { TemporaryService } from 'src/app/_services/temporary.service';

@Component({
  selector: 'arena-zone',
  templateUrl: './zone.component.html',
  styleUrls: ['./zone.component.css']
})
export class ZoneComponent implements OnInit {
  @Input() cards: Array<InGameCard> | Array<any>; 
  @Input() height: String;
  @Input() type: String;
  @Input() zoneObject : Object;

  @Output() moveC: EventEmitter<Object> = new EventEmitter();
  @Output() setAct: EventEmitter<PlayerCard> = new EventEmitter();
  @Output() summon: EventEmitter<number> = new EventEmitter();
  @Output() rotateM: EventEmitter<number> = new EventEmitter();
  @Output() selectC: EventEmitter<Object> = new EventEmitter();
  @Output() attackC: EventEmitter<any> = new EventEmitter();

  constructor(private temp: TemporaryService) { }

  ngOnInit() {
  }

  style(card: InGameCard, index: number): Object {
    let style: Object = {};

    if (this.type == "ofield" || this.type == "field") {
      let newStyle: Object = { 'margin-right' : this.cards.length < 8 ? 0 : ((this.cards.length-4)*-8) + 'px', 'height' : '80%'};
      style = Object.assign(style, newStyle);
    }

    if(this.type == "mana") {
      let newStyle: Object = { 'margin-right' : this.cards.length < 10 ? 0 : ((this.cards.length-4)*-2) + 'px'};
      style = Object.assign(style, newStyle);
    }

    if(this.type == "omana") {
      let newStyle: Object = { 'margin-right' : this.cards.length < 10 ? 0 : ((this.cards.length-4)*-1) + 'px'};
      style = Object.assign(style, newStyle);
    }

    if(card.need_select) {
      let newStyle: Object = { 'border' : '2px solid orange', 'margin-top' : '-2px' };
      style = Object.assign(style, newStyle);
    }

    if(card.selected) {
      let newStyle: Object = { 'border' : '2px solid green', 'margin-top' : '-2px' };
      style = Object.assign(style, newStyle);
    }

    if(this.type == "field" && index == this.temp.attackWithIndex) {
      let newStyle: Object = { 'border' : '2px solid blue', 'margin-top' : '-2px' };
      style = Object.assign(style, newStyle);
    }

    if(this.type == "ofield" && index == this.temp.targetIndex) {
      let newStyle: Object = { 'border' : '2px solid red', 'margin-top' : '-2px' };
      style = Object.assign(style, newStyle);
    }
 
    return style;
  }

  moveCard(id: number, index: number, from: string, to: string)
  {
    this.moveC.emit({
      "id" : id,
      "index" : index,
      "from" : from,
      "to" : to
    });
  }

  setActive(card: PlayerCard)
  {
    this.setAct.emit(card);
  }

  canSummon(index: number) : boolean
  {
    let card: InGameCard = this.cards[index];
    
    return this.zoneObject["inManaSummon"]["Total"] >= card.details.cost &&
           this.zoneObject["inManaSummon"][card.details.civilization] >= 1;
  }

  summonC(index: number)
  {
    this.summon.emit(index);
  }

  rotateMana(index: number)
  {
    this.rotateM.emit(index);
  }

  select(index: number)
  {
    let owner = "me";
    if(this.type == "omana" || this.type == "ofield")
      owner = "opponent";
    this.selectC.emit({
      "index" : index,
      "owner" : owner,
      "type" : this.type
    });
  }

  attackWith(index: number)
  {
    this.temp.attackWithIndex = index;
  }

  attack(target: any)
  {
    if(target != "shield")
      this.temp.targetIndex = target;
    this.attackC.emit({"attacker" : this.temp.attackWithIndex, "target" : target});
  }
}
