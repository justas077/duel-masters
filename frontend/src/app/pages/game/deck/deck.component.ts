import { Component, OnInit } from '@angular/core';
import { DeckService } from '../../../_services/deck.service';
import { Card } from '../../../_shared/models/card.model';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { PlayerCard } from '../../../_shared/models/player-card.model';

@Component({
  selector: 'app-deck',
  templateUrl: './deck.component.html',
  styleUrls: ['./deck.component.css']
})
export class DeckComponent implements OnInit {

  constructor(private deckService: DeckService) {
    this.deckService.getDeck();
    this.deckService.getCards();
  }

  ngOnInit() {
    
  }

  setActiveCard(index: number, where: string)
  {
    this.deckService.setActiveCard(index, where);
  }

  add(i: number)
  {
    let card: PlayerCard = this.deckService.cards[i];
    let deckCardCopy: PlayerCard = {...card};
    let insert: number = 0;
    let amount: number = 1;

    this.deckService.deck.forEach((deckCard: PlayerCard, index) => {
      if(deckCard.card_id === card.card_id) {
        deckCardCopy = {...deckCard};
        insert = index;
        deckCard.amount++;
        amount = deckCard.amount;
      }
    });

    console.log(amount);
    this.deckService.deck.splice(insert, 0, deckCardCopy);
    this.deckService.deck[insert].amount = amount;
    card.amount--;
  }

  remove(i: number)
  {
    let card: PlayerCard = this.deckService.deck[i];
    let insert: boolean = true;

    this.deckService.deck.forEach((deckCard: PlayerCard, index) => {
      if(deckCard.card_id === card.card_id) {
          deckCard.amount--;
      }
    });

    this.deckService.cards.forEach((cCard: PlayerCard, cIndex) => {
      if(cCard.card_id === card.card_id) {
          cCard.amount++;
          insert = false;
      }
    });

    if(insert) {
      this.deckService.cards.splice(0, 0, card);
      this.deckService.cards[0].amount = 1;
    }

    this.deckService.deck.splice(i, 1);
  }
}
