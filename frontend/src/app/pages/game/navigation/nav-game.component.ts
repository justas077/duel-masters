import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DeckService } from '../../../_services/deck.service';
import { GameComponent } from '../game.component';

@Component({
  selector: 'nav-game',
  templateUrl: './nav-game.component.html',
  styleUrls: ['./nav-game.css']
})
export class NavGameComponent {

  constructor(private router: Router, private deckService: DeckService, private gameComponent: GameComponent) 
  {
  }

  saveDeck()
  {
    this.deckService.saveDeck();
  }

  searchForgame()
  {
    this.gameComponent.search = true;
    this.gameComponent.searchGame();
  }
}
