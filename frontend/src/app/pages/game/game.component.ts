import { Component, OnInit } from '@angular/core';
import { RequestsService } from 'src/app/_services/requests.service';
import { PusherService } from 'src/app/_services/pusher.service';
import { GameService } from 'src/app/_services/game.service';
import { Router } from '@angular/router';
import { DeckService } from '../../_services/deck.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  status = null;
  search = false;

  constructor(private requestService: RequestsService,
              private pusherService: PusherService,
              private gameService: GameService,
              private router: Router,
              private deckService: DeckService) { }

  ngOnInit() {
  }

  searchGame()
  {
    this.search = true;
    this.status = "creating session";
    this.requestService.searchGame().subscribe(
      (data: any) => {
        this.status = "searching";
        if(data["first_player"]) {
          this.pusherService.subscribeSearchChannel().bind("search", (pusherData: any) => {
            this.status = "starting";
            this.pusherService.unsubscribeSearchChannel();
            this.gameService.setGame();
            this.router.navigate(['game/arena']);
          });
        } else {
          this.status = "starting";
            this.gameService.setGame();
            this.router.navigate(['game/arena']);
        }
      }
    );
  }

}
