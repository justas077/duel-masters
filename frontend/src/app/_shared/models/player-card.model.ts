import { Card } from './card.model';

export class PlayerCard
{
    id: number;
    card_id: number;
    details: Card;
    amount: number;
}