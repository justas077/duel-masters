export class Effect
{
    id: number;
    function_name: string;
    activation_time: string;
    select: string;
    select_own_included: boolean;
    is_response_need: boolean;
    amount: number;
    is_up_to: boolean;
}