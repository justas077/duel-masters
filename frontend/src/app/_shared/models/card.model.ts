import { Effect } from './effect.model';

export class Card
{
    id: number;
    name: string;
    civilization: string;
    race: string;
    type: string;
    cost: number;
    power: number;
    flavor_text: string;
    rarity: string;
    artist: string;
    image_address: string;
    effect: Effect;
}