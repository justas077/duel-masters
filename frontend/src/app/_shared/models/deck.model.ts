import { Card } from './card.model';
import { PlayerCard } from './player-card.model';

export class Deck
{
    id: number;
    cards_count: number;
    is_active: boolean;
    cards: Array<PlayerCard>;
    amounts: Array<any>;
}