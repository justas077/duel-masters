import { Card } from './card.model';

export class InGameCard
{
    id: number;
    details: Card;
    is_tapped: boolean;
    is_used_for_summon: boolean;
    need_select: boolean;
    selected: boolean;
    card_id: number;
    is_summon: boolean;
}