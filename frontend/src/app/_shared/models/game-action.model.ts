export class GameAction
{
    action: string;
    actionParameters: Array<any>;
    owner: string;
}