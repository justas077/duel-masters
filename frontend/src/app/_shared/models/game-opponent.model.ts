import { InGameCard } from './in-game-card.model';

export class GameOpponent
{
    cards_in_hand: number;
    cards_in_mana: number;
    cards_in_graveyard: number;
    cards_in_extra_deck: number;
    cards_in_field: number;
    cards_in_shield: number;
    cards_in_deck: number;
    mana: Array<InGameCard>;
    field: Array<InGameCard>;
    graveyard: Array<InGameCard>;
    need_select: boolean;
}