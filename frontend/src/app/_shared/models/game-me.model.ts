import { InGameCard } from './in-game-card.model';

export class GameMe
{
    cards_in_hand: number;
    cards_in_mana: number;
    cards_in_graveyard: number;
    cards_in_extra_deck: number;
    cards_in_field: number;
    cards_in_shield: number;
    is_added_to_mana: boolean;
    hand: Array<InGameCard>;
    mana: Array<InGameCard>;
    graveyard: Array<InGameCard>;
    field: Array<InGameCard>;
    is_my_turn: boolean;
    in_mana_summon: Array<any>;
    need_select: Array<InGameCard>;
}