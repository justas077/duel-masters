import { GameMe } from './game-me.model';
import { GameOpponent } from './game-opponent.model';

export class Game
{
    me: GameMe;
    opponent: GameOpponent;
    constructor() {
        this.me = new GameMe();
        this.opponent = new GameOpponent();
    }
}