export interface Pagination<T> {
    current_page:number,
    data: Array<T>,
    last_page: number
}