import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ArenaComponent } from './pages/duel/arena/arena.component';
import { AuthService } from './_services/auth.service';
import { AuthGuard } from './_shared/guards/auth.guard';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { TokenInterceptorService } from './_services/token-interceptor.service';
import { LoginComponent } from './pages/main/login/login.component';
import { AppRoutingModule } from './app.routing';
import { RequestsService } from './_services/requests.service';
import { GameService } from './_services/game.service';
import { PusherService } from './_services/pusher.service';
import { ReactiveFormsModule } from '@angular/forms';
import { GameComponent } from './pages/game/game.component';
import { MainComponent } from './pages/main/main.component';
import { DeckComponent } from './pages/game/deck/deck.component';
import { DeckService } from './_services/deck.service';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NavGameComponent } from './pages/game/navigation/nav-game.component';
import { EffectsService } from './_services/effects.service';
import { ZoneComponent } from './pages/duel/arena/zone/zone.component';
import { TemporaryService } from './_services/temporary.service';

@NgModule({
  declarations: [
    AppComponent,
    ArenaComponent,
    LoginComponent,
    GameComponent,
    MainComponent,
    DeckComponent,
    NavGameComponent,
    ZoneComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    DragDropModule
  ],
  providers: [
    AuthService,
    AuthGuard, 
    RequestsService, 
    GameService, 
    PusherService,
    DeckService,
    EffectsService,
    TemporaryService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
