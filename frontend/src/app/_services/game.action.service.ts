import { Injectable } from '@angular/core';
import { PusherService } from './pusher.service';
import { GameAction } from '../_shared/models/game-action.model';
import { GameService } from './game.service';
import { InGameCard } from '../_shared/models/in-game-card.model';

@Injectable()
export class GameActionService {

    constructor(private pusherService: PusherService, private gameService: GameService)
    {
        this.pusherService.subscribeDuelPlayerChannel().bind("duel-action", (data: GameAction) => {
            console.log(data);
            if(data.action == "MOVE") {
                this.move(data);
            } else if (data.action == "MANA-ROTATE") {
                this.rotateMana(data);
            } else if(data.action == "ROTATE") {
                this.rotate(data);
            } else if(data.action == "START-TURN") {
                this.gameService.setGame();
            }
        });
    }

    private move(moveData: GameAction) 
    {
        let from = moveData.actionParameters["from"];
        let to = moveData.actionParameters["to"];
        let card = moveData.actionParameters["card"];
        let owner = moveData.owner;

        if(from != "hand" && from != "shield") {
            this.gameService[owner][from].forEach((c: InGameCard, index: number) => {
                if(c.id == card["id"]) {
                    this['gameService'][owner][from].splice(index, 1);
                }
            });
        }
        this['gameService'][owner][to].push(card);
        this['gameService'][owner]['cards_in_' + from]--;
        this['gameService'][owner]['cards_in_' + to]++;
    }

    private rotateMana(rotateData: GameAction)
    {
        let rotate = rotateData.actionParameters["rotate"];
        this.gameService.opponent.mana.forEach((card: InGameCard) => {
            if(card.id in rotate) {
                card.is_tapped = rotate[card.id];
            }
        });
    }

    private rotate(rotateData: GameAction)
    {
        let rotateId = rotateData.actionParameters["rotate"];
        let owner = rotateData.owner;
        let zone = rotateData.actionParameters["zone"];
        console.log(rotateId);
        this.gameService[owner][zone].forEach((card: InGameCard) => {
            console.log(card.id);
            if(card.id == rotateId) {
                card.is_tapped = !card.is_tapped;
            }
        });
    }

}