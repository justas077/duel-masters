import { InGameCard } from '../_shared/models/in-game-card.model';
import { GameService } from './game.service';
import { Injectable } from '@angular/core';

@Injectable()
export class EffectsService {
    constructor(private gameService: GameService) { }

    moveCard(index: number, owner: string, from: string, to: string)
    {
        let card: InGameCard = this['gameService'][owner][from][index];
        this['gameService'][owner][from].splice(index, 1);
        this['gameService'][owner][to].push(card);
        this['gameService'][owner]['cards_in_' + from]--;
        this['gameService'][owner]['cards_in_' + to]++;
    }

    return_to_hand(card: InGameCard, selected: Object)
    {
        let keys = Object.keys(selected);
        for (let id of keys) {
            let from = card.details.effect.select;
            this.gameService[selected[id]][from].forEach((c: InGameCard, index: number) => {
                if(c.card_id.toString() == id)
                    this.moveCard(index, selected[id], from, "hand");
            });
        }
    }
}