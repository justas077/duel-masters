import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Router } from '@angular/router'

@Injectable()
export class AuthService {
  constructor(private _router: Router) { }

  logoutUser() {
    localStorage.removeItem('token')
    //this._router.navigate(['/events'])
  }

  getToken() {
    return localStorage.getItem('access_token')
  }

  getUser() {
    return localStorage.getItem('current_user')
  }

  loggedIn() {
    return !!localStorage.getItem('access_token') && !!localStorage.getItem('current_user');
  }
}
