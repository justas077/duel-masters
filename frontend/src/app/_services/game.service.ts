import { Injectable } from '@angular/core';
import { RequestsService } from './requests.service';
import { Observable } from 'rxjs';
import { Game } from '../_shared/models/game.model';
import { GameMe } from '../_shared/models/game-me.model';
import { Card } from '../_shared/models/card.model';
import { GameOpponent } from '../_shared/models/game-opponent.model';
import { InGameCard } from '../_shared/models/in-game-card.model';
import { TemporaryService } from './temporary.service';

@Injectable()
export class GameService {

    public game: Game = new Game();
    public me: GameMe = new GameMe();
    public opponent: GameOpponent = new GameOpponent();
    public isMyTurn = false;

    constructor(private requests: RequestsService, private temporaryService: TemporaryService) {
        
    }

    setGame()
    {
        this.requests.getGame().subscribe(
            (data: Game) => {
                this.game = data; 
                this.me = this.game.me;
                this.opponent = this.game.opponent;
                this.isMyTurn = this.me.is_my_turn;
                console.log(this.game);
            }
        );
    }

    moveCard(card: number, from: string, to: string)
    {
        this.requests.moveCard(card, from, to).subscribe(
            (data: any) => { console.log("Card moved"); }
        );
    }

    isMyTurn_f() : string
    {
        if(this.isMyTurn)
            return "It's Your Turn.";
        return "Opponent turn."
    }

    confirmMana()
    {
        this.requests.confirmMana(this.game.me.mana).subscribe(
            (data: any) => { console.log("Mana rotated."); }
        );
    }

    summon(card: number, selected: Object = null)
    {
        this.requests.summon(card, selected).subscribe(
            (data: any) => { console.log("Summoned."); }
        );
    }

    attack(attackerId: number, target: number|string, attIndex: number, deffIndex: number)
    {
        this.requests.attack(attackerId, target).subscribe(
            (data: any) => {
                let res = data["message"];
                if(res == "win") {
                    this.moveC(deffIndex, "opponent", "field", "graveyard");
                } else if(res == "lose") {
                    this.moveC(attIndex, "me", "field", "graveyard");
                } else if(res == "shield_braked") {
                    this.removeShield();
                } else {
                    this.moveC(deffIndex, "opponent", "field", "graveyard");
                    this.moveC(attIndex, "me", "field", "graveyard");
                }
                this.temporaryService.attackWithIndex = null;
                this.temporaryService.targetIndex = null;
            }
        );
    }

    endTurn()
    {
        this.isMyTurn = false;
        this.opponent.cards_in_hand++;
        this.opponent.cards_in_deck--;
        this.opponent.mana.forEach((card: InGameCard) => {
            card.is_tapped = false;
            card.is_used_for_summon = false;
        });
        this.opponent.field.forEach((card: InGameCard) => {
            card.is_tapped = false;
        });
        this.requests.endTurn().subscribe(
            (data: any) => { console.log("Turn ended."); }
        );
    }

    moveC(index: number, owner: string, from: string, to: string)
    {
        console.log(index + " " + owner + " " + from + " " + to);
        let card: InGameCard = this[owner][from][index];
        this[owner][from].splice(index, 1);
        this[owner][to].push(card);
        this[owner]['cards_in_' + from]--;
        this[owner]['cards_in_' + to]++;
    }

    removeShield()
    {
        this.opponent.cards_in_hand++;
        this.opponent.cards_in_shield--;
    }
}