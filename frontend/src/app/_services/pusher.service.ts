import { Injectable } from '@angular/core';
import * as Pusher from 'pusher-js';
import { AuthService } from './auth.service';

const APP_ID = "705212";
const APP_KEY = "cce2532897b3f2790297"
const APP_SECRET = "191d8f911f1de2272f0c"
const APP_CLUSTER = "eu"
const AUTH_ENDPOINT = "http://duelmasters.game/broadcasting/auth";

@Injectable()
export class PusherService {

    pusher: any;
    constructor(private authService: AuthService) {
        this.pusher = new Pusher(APP_KEY, {
          cluster: APP_CLUSTER,
          encrypted: true,
          authEndpoint: AUTH_ENDPOINT,
          auth: {
            headers: {
                Authorization: 'Bearer ' + authService.getToken()
            },
        },
        });
    }

    subscribeDuelPlayerChannel()
    {
        return this.pusher.subscribe('private-duel-player.' + this.authService.getUser());
    }

    subscribeSearchChannel()
    {
        return this.pusher.subscribe('private-game-search.' + this.authService.getUser());
    }

    unsubscribeSearchChannel()
    {
        return this.pusher.unsubscribe('private-game-search.' + this.authService.getUser());
    }
    
}
