import { Injectable } from '@angular/core';
import { RequestsService } from './requests.service';
import { Deck } from '../_shared/models/deck.model';
import { Card } from '../_shared/models/card.model';
import { PlayerCard } from '../_shared/models/player-card.model';


@Injectable()
export class DeckService {

    public deck: Array<PlayerCard> = [];
    public amounts: Array<any> = [];
    public isLoadingDeck = true;

    public cards: Array<PlayerCard> = [];
    public isLoadingCards = true;

    public activeCard: Card | PlayerCard = null;

    constructor(private requestService: RequestsService) {
    }

    getDeck()
    {
        this.requestService.deck().subscribe(
            (data: Deck) => {
                this.deck = data.cards;
                this.isLoadingDeck = false;
                this.amounts = data.amounts;
                this.updateAmounts();
            }
        );
    }

    getCards()
    {
        this.requestService.playerCards().subscribe(
            (data: Array<PlayerCard>) => {
                this.cards = data;
                this.isLoadingCards = false;
                this.updateAmounts();
            }
        );
    }

    setActiveCard(index: number, where: string)
    {
        if(where == 'deck')
            this.activeCard = this.deck[index];

        if(where == 'cards')
            this.activeCard = this.cards[index];
    }

    updateAmounts()
    {
        if(!this.isLoadingCards && !this.isLoadingDeck) {
            this.cards.forEach((cCard: PlayerCard) => {
                if(cCard.card_id in this.amounts)
                    cCard.amount -= this.amounts[cCard.card_id];
            });
        }
    }

    saveDeck()
    {
        this.requestService.saveDeck(this.deck).subscribe(
            (data: any) => { console.log("saved."); }
        );;
    }
}
