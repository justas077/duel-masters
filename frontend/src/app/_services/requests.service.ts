import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs';
import { Pagination } from "../_shared/interfaces/pagination-interface";
import { TokenResponse } from "../_shared/interfaces/token-response-interface";
import { User } from '../_shared/models/user.model';
import { Game } from '../_shared/models/game.model';
import { InGameCard } from '../_shared/models/in-game-card.model';
import { Deck } from '../_shared/models/deck.model';
import { PlayerCard } from '../_shared/models/player-card.model';

const BACKEND_URL = "http://duelmasters.game/";
const CLIENT_ID = 2;
const CLIENT_SECRET = "RQ6PdXxhPZOkBRsgq53YXyKkSoJBIc0EPjcb6Kmz";

@Injectable()
export class RequestsService
{
    constructor(private httpClient: HttpClient) {}

    getGame(): Observable<Game>
    {
        return this.httpClient.get<Game>(BACKEND_URL + 'api/game');
    }

    getCurrentUser(): Observable<User>
    {
        return this.httpClient.get<User>(BACKEND_URL + 'api/user');
        //.publishLast().refCount()
    }

    getToken(username: string, password: string): Observable<TokenResponse> {
        return this.httpClient.post<TokenResponse>(BACKEND_URL+'oauth/token', {
            "username" : username,
            "password" : password,
            "client_id" : CLIENT_ID,
            "client_secret" : CLIENT_SECRET,
            "grant_type" : "password",
            "scope" : "*"
        });
    }

    searchGame()
    {
        return this.httpClient.get(BACKEND_URL + 'api/game/search');
    }

    moveCard(card: number, from: string, to: string): Observable<any> 
    {
        return this.httpClient.put(BACKEND_URL + 'api/game/move-card/' + card, {
            "from" : from,
            "to" : to
        });
    } 

    confirmMana(mana: Array<InGameCard>): Observable<any>
    {
        return this.httpClient.put<Array<InGameCard>>(BACKEND_URL + 'api/game/confirm-mana', {"mana" : mana});
    }

    summon(card: number, selected: Object): Observable<any>
    {
        return this.httpClient.put(BACKEND_URL + 'api/game/summon/' + card, {"selected" : selected});
    }

    attack(attacker: number, target: any): Observable<any>
    {
        return this.httpClient.put(BACKEND_URL + 'api/game/attack/' + attacker, {"target" : target});
    }

    endTurn(): Observable<any>
    {
        return this.httpClient.get(BACKEND_URL + 'api/game/end-turn');
    }

    deck(): Observable<Deck>
    {
        return this.httpClient.get<Deck>(BACKEND_URL + 'api/deck');
    }

    playerCards(): Observable<Array<PlayerCard>>
    {
        return this.httpClient.get<Array<PlayerCard>>(BACKEND_URL + 'api/player-cards');
    }

    saveDeck(deck: Array<PlayerCard>): Observable<any>
    {
        return this.httpClient.put(BACKEND_URL + 'api/save-deck', {"deck" : deck});
    }

}