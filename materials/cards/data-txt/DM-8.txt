Duel Masters: Epic Dragons of Hyperchaos Card List Wizards/Shogakukan/Mitsui-Kids.
Duel Masters: Epic Dragons of Hyperchaos released October 2005

Number: 60 cards total
Civilizations:  12 darkness, 12 fire, 12 light, 12 nature, 12 water
Rarity: 20 common, 15 uncommon, 15 rare, 5 very rare, 5 super rare

Card Name:	Emperor Quazla
Civilization:	Water
Type:		Evolution Creature
Race:		Cyber Lord
Cost:		6
Power:		5000
Rules Text:	[Blocker]  Blocker (Whenever an opponent's creature attacks, you may tap this creature to stop the attack. Then the 2 creatures battle.)
		Evolution - Put on one of your Cyber Lords.
		Whenever your opponent uses the "shield trigger" ability of one of his shields, draw up to 2 cards.
Flavor Text:	
Rarity:		S
Collector #:	S2
Artist:		Toshiaki Takayama

Card Name:	Nastasha, Channeler of Suns
Civilization:	Light
Type:		Creature
Race:		Mecha del Sol
Cost:		7
Power:		6000
Rules Text:	Double breaker (This creature breaks 2 shields.)
		When one of your shields would be broken, you may destroy this creature instead.
Flavor Text:	The universe trembles at its beauty.
Rarity:		S
Collector #:	S1
Artist:		Masaki Hirooka

Card Name:	Super Necrodragon Abzo Dolba
Civilization:	Darkness
Type:		Evolution Creature
Race:		Zombie Dragon
Cost:		6
Power:		11000+
Rules Text:	Evolution - Put on one of your creatures that has Dragon in its race.
		This creature gets +2000 power for each creature in your graveyard.
		Triple breaker (This creature breaks 3 shields.)
Flavor Text:	The Dragons rose. Hopes fell.
Rarity:		S
Collector #:	S3
Artist:		Ittoku

Card Name:	Super Terradragon Bailas Gale
Civilization:	Nature
Type:		Evolution Creature
Race:		Earth Dragon
Cost:		5
Power:		9000
Rules Text:	Evolution - Put on one of your creatures that has Dragon in its race.
		After you cast a spell by using its "shield trigger" ability, put it into your hand instead of your graveyard.
		Double breaker (This creature breaks 2 shields.)
Flavor Text:	
Rarity:		S
Collector #:	S5
Artist:		Taro Yamazaki

Card Name:	Uberdragon Bajula
Civilization:	Fire
Type:		Evolution Creature
Race:		Armored Dragon
Cost:		7
Power:		13000
Rules Text:	Evolution - Put on one of your creatures that has Dragon in its race.
		Whenever this creature attacks, choose up to 2 cards in your opponent's mana zone. Your opponent puts those cards into his graveyard.
		Triple breaker (This creature breaks 3 shields.)
Flavor Text:	
Rarity:		S
Collector #:	S4
Artist:		Seki

Card Name:	Kuukai, Finder of Karma
Civilization:	Light
Type:		Evolution Creature
Race:		Mecha Thunder
Cost:		5
Power:		10500
Rules Text:	[Blocker]  Blocker (Whenever an opponent's creature attacks, you may tap this creature to stop the attack. Then the 2 creatures battle.)
		Evolution - Put on one of your Mecha Thunders.
		Whenever this creature blocks, untap it after it battles.
		This creature can't attack players.
Flavor Text:	
Rarity:		V
Collector #:	1
Artist:		Yarunoca

Card Name:	Aqua Ranger
Civilization:	Water
Type:		Creature
Race:		Liquid People
Cost:		6
Power:		3000
Rules Text:	This creature can't be blocked.
		When this creature would be destroyed, put it into your hand instead.
Flavor Text:	"I'm on a special mission to the surface to bring back secret computer codes, enemy troop movements, and a case of metal polish."
Rarity:		V
Collector #:	2
Artist:		Atsushi Kawasaki

Card Name:	Megaria, Empress of Dread
Civilization:	Darkness
Type:		Creature
Race:		Dark Lord
Cost:		5
Power:		5000
Rules Text:	Each creature in the battle zone has "slayer." (Whenever a creature that has "slayer" battles, destroy the other creature after the battle.)
Flavor Text:	Before the eerily silent army, she raised the chalice of souls. "This time," she snarled, "it better be filled with diet souls."
Rarity:		V
Collector #:	3
Artist:		Tsuu

Card Name:	Magmadragon Jagalzor
Civilization:	Fire
Type:		Creature
Race:		Volcano Dragon
Cost:		6
Power:		6000
Rules Text:	Double breaker (This creature breaks 2 shields.)
		Turbo rush (If any of your other creatures broke any shields this turn, this creature gets its [TurboRush]  ability until the end of the turn.)
		[TurboRush] : Each of your creatures in the battle zone has "speed attacker." (A creature that has "speed attacker" doesn't get summoning sickness.)
Flavor Text:	
Rarity:		V
Collector #:	4
Artist:		Naoki Saito

Card Name:	Kachua, Keeper of the Icegate
Civilization:	Nature
Type:		Creature
Race:		Snow Faerie
Cost:		7
Power:		3000
Rules Text:	Instead of having this creature attack, you may tap it to use its [Tap] ability.
		[Tap]: Search your deck. You may take a creature that has Dragon in its race from your deck and put it into the battle zone. Then shuffle your deck. That creature has "speed attacker." At the end of the turn, destroy it.
Flavor Text:	
Rarity:		V
Collector #:	5
Artist:		Eiji Kaneda

Card Name:	Nariel, the Oracle
Civilization:	Light
Type:		Creature
Race:		Light Bringer
Cost:		4
Power:		1000
Rules Text:	Creatures that have power 3000 or more can't attack. (Creatures that have power less than 3000 and get extra power while attacking can still attack.)
Flavor Text:	Just as predicted, the earthquakes created by the waking Dragons reactivated the sentinels. Everything was still under their control.
Rarity:		R
Collector #:	11
Artist:		Atsushi Kawasaki

Card Name:	Sasha, Channeler of Suns
Civilization:	Light
Type:		Creature
Race:		Mecha del Sol
Cost:		8
Power:		9500+
Rules Text:	[Blocker]  Dragon blocker (Whenever an opponent's creature that has Dragon in its race attacks, you may tap this creature to stop the attack. Then the 2 creatures battle.)
		While battling a creature that has Dragon in its race, this creature gets +6000 power.
		Double breaker (This creature breaks 2 shields.)
Flavor Text:	
Rarity:		R
Collector #:	12
Artist:		Jason

Card Name:	Thrumiss, Zephyr Guardian
Civilization:	Light
Type:		Creature
Race:		Guardian
Cost:		6
Power:		3000
Rules Text:	Whenever any of your creatures attacks, you may choose one of your opponent's creatures in the battle zone and tap it. (First choose what your creature is attacking. Then choose a creature to tap.)
Flavor Text:	"Catch my drift?"
Rarity:		R
Collector #:	15
Artist:		Hisashi Momose

Card Name:	Aqua Grappler
Civilization:	Water
Type:		Creature
Race:		Liquid People
Cost:		5
Power:		3000
Rules Text:	Whenever this creature attacks, you may draw a card for each other tapped creature you have in the battle zone.
Flavor Text:	"I specialize in kicking! And jumping! And sometimes spinning and whirling! I know my mom spent all that money on grappling lessons, but that's just not what I'm into anymore."
Rarity:		R
Collector #:	16
Artist:		Hisashi Momose

Card Name:	Marine Scramble
Civilization:	Water
Type:		Spell
Race:		
Cost:		7
Power:		0
Rules Text:	Your creatures in the battle zone can't be blocked this turn.
Flavor Text:	"Analysis complete. Begin the assault!" -Emperor Quazla
Rarity:		R
Collector #:	22
Artist:		Jason

Card Name:	Vikorakys
Civilization:	Water
Type:		Creature
Race:		Sea Hacker
Cost:		3
Power:		1000
Rules Text:	Turbo rush (If any of your other creatures broke any shields this turn, this creature gets its [TurboRush]  ability until the end of the turn.)
		[TurboRush] : When this creature attacks, search your deck. You may take a card from your deck and put it into your hand. Then shuffle your deck.
Flavor Text:	
Rarity:		R
Collector #:	24
Artist:		Ittoku

Card Name:	Cranium Clamp
Civilization:	Darkness
Type:		Spell
Race:		
Cost:		4
Power:		0
Rules Text:	Your opponent chooses and discards 2 cards from his hand.
Flavor Text:	"Hold still! This is the only headache remedy I know. I promise, soon you won't be able to feel your head at all!"
Rarity:		R
Collector #:	27
Artist:		Akifumi Yamamoto

Card Name:	Necrodragon Galbazeek
Civilization:	Darkness
Type:		Creature
Race:		Zombie Dragon
Cost:		6
Power:		9000
Rules Text:	Whenever this creature attacks, choose one of your shields and put it into your graveyard.
		Double breaker (This creature breaks 2 shields.)
Flavor Text:	Its wingbeats whisper promises of doom.
Rarity:		R
Collector #:	32
Artist:		Ryoya Yuki

Card Name:	Scream Slicer, Shadow of Fear
Civilization:	Darkness
Type:		Creature
Race:		Ghost
Cost:		6
Power:		4000
Rules Text:	Whenever you put a Dragonoid or a creature that has Dragon in its race into the battle zone, destroy the creature that has the least power in the battle zone. If there's a tie, you choose from among the tied creatures.
Flavor Text:	Neither flesh nor blood nor souls can ever slake the thirst of the dead.
Rarity:		R
Collector #:	34
Artist:		Yoko Tsukamoto

Card Name:	Furious Onslaught
Civilization:	Fire
Type:		Spell
Race:		
Cost:		4
Power:		0
Rules Text:	Until the end of the turn, each of your Dragonoids in the battle zone is an Armored Dragon in addition to its other races, gets +4000 power, and has "double breaker (This creature breaks 2 shields)."
Flavor Text:	"See what happens when you take your vitamins?" -Pyrofighter Magnus
Rarity:		R
Collector #:	37
Artist:		Norikatsu Miyoshi

Card Name:	Kyrstron, Lair Delver
Civilization:	Fire
Type:		Creature
Race:		Dragonoid
Cost:		5
Power:		1000
Rules Text:	When this creature is destroyed, you may put a creature that has Dragon in its race from your hand into the battle zone.
Flavor Text:	"Sneak in, grab some Dragon eggs, sneak out, and make one enormous omelet. What could be simpler?"
Rarity:		R
Collector #:	38
Artist:		Ken Sugawara

Card Name:	Slaphappy Soldier Galback
Civilization:	Fire
Type:		Creature
Race:		Dragonoid
Cost:		4
Power:		3000
Rules Text:	Turbo rush (If any of your other creatures broke any shields this turn, this creature gets its [TurboRush]  ability until the end of the turn.)
		[TurboRush] : Whenever this creature attacks, you may destroy one of your opponent's creatures that has power 4000 or less.
Flavor Text:	
Rarity:		R
Collector #:	42
Artist:		Masaki Hirooka

Card Name:	Carbonite Scarab
Civilization:	Nature
Type:		Creature
Race:		Giant Insect
Cost:		4
Power:		3000
Rules Text:	Turbo rush (If any of your other creatures broke any shields this turn, this creature gets its [TurboRush]  ability until the end of the turn.)
		[TurboRush] : Whenever this creature is attacking your opponent and becomes blocked, it breaks one of your opponent's shields.
Flavor Text:	
Rarity:		R
Collector #:	47
Artist:		Kou1

Card Name:	Root Charger
Civilization:	Nature
Type:		Spell
Race:		
Cost:		3
Power:		0
Rules Text:	Whenever any of your creatures would be destroyed this turn, put it into your mana zone instead.
		Charger (After you cast this spell, put it into your mana zone instead of your graveyard.)
Flavor Text:	Life is the building block of life.
Rarity:		R
Collector #:	52
Artist:		Atsushi Kawasaki

Card Name:	Terradragon Gamiratar
Civilization:	Nature
Type:		Creature
Race:		Earth Dragon
Cost:		4
Power:		6000
Rules Text:	When you put this creature into the battle zone, your opponent may choose a creature in his hand and put it into the battle zone.
		Double breaker (This creature breaks 2 shields.)
Flavor Text:	Quazla's doomsday device caused the planet's core to crack. That was the alarm clock the Dragons had been waiting for.
Rarity:		R
Collector #:	54
Artist:		Mikumo

Card Name:	Dracobarrier
Civilization:	Light
Type:		Spell
Race:		
Cost:		3
Power:		0
Rules Text:	[Shield trigger]  Shield trigger (When this spell is put into your hand from your shield zone, you may cast it immediately for no cost.)
		Choose one of your opponent's creatures in the battle zone and tap it. If it has Dragon in its race, add the top card of your deck to your shields face down.
Flavor Text:	
Rarity:		U
Collector #:	6
Artist:		Jason

Card Name:	Laser Whip
Civilization:	Light
Type:		Spell
Race:		
Cost:		4
Power:		0
Rules Text:	Choose one of your opponent's creatures in the battle zone and tap it. Then you may choose one of your creatures in the battle zone. If you do, it can't be blocked this turn.
Flavor Text:	"No, no, no! It's not a laser! It's a high-intensity, focused, optical beam! Of course, it needs a laser to work." -Ouks, Vizier of Restoration
Rarity:		U
Collector #:	7
Artist:		Takesi Kuno

Card Name:	Solar Grass
Civilization:	Light
Type:		Creature
Race:		StarLight Tree
Cost:		5
Power:		3000
Rules Text:	Turbo rush (If any of your other creatures broke any shields this turn, this creature gets its [TurboRush]  ability until the end of the turn.)
		[TurboRush] : Whenever this creature is attacking your opponent and isn't blocked, untap all your creatures in the battle zone except Solar Grasses.
Flavor Text:	
Rarity:		U
Collector #:	14
Artist:		Katsuhiko Kojoh

Card Name:	Lalicious
Civilization:	Water
Type:		Creature
Race:		Sea Hacker
Cost:		6
Power:		4000
Rules Text:	Whenever this creature attacks, you may look at your opponent's hand and at the top card of his deck.
Flavor Text:	Its artificial eye has X-ray vision, Y-ray vision, and Z-ray vision.
Rarity:		U
Collector #:	21
Artist:		Ken Sugawara

Card Name:	Prowling Elephish
Civilization:	Water
Type:		Creature
Race:		Gel Fish
Cost:		4
Power:		2000
Rules Text:	[Blocker]  Blocker (Whenever an opponent's creature attacks, you may tap this creature to stop the attack. Then the 2 creatures battle.)
Flavor Text:	"I knew the laws of nature could bend. I didn't know they could be twisted into a pretzel." -Mini Titan Gett
Rarity:		U
Collector #:	23
Artist:		Fruit Parfait

Card Name:	Wave Lance
Civilization:	Water
Type:		Spell
Race:		
Cost:		3
Power:		0
Rules Text:	Choose a creature in the battle zone and return it to its owner's hand. If it has Dragon in its race, you may draw a card.
Flavor Text:	Even the ruler of the skies cannot best the waters.
Rarity:		U
Collector #:	25
Artist:		Taro Yamazaki

Card Name:	Dimension Splitter
Civilization:	Darkness
Type:		Creature
Race:		Brain Jacker
Cost:		3
Power:		1000
Rules Text:	When you put this creature into the battle zone, you may return all creatures that have Dragon in their race from your graveyard to your hand.
Flavor Text:	They think life is a joke, death is a joke, and mixing the two up is a really funny joke.
Rarity:		U
Collector #:	28
Artist:		Katsuhiko Kojoh

Card Name:	Gachack, Mechanical Doll
Civilization:	Darkness
Type:		Creature
Race:		Death Puppet
Cost:		3
Power:		2000
Rules Text:	Turbo rush (If any of your other creatures broke any shields this turn, this creature gets its [TurboRush]  ability until the end of the turn.)
		[TurboRush] : Whenever this creature is attacking your opponent and isn't blocked, you may destroy a creature.
Flavor Text:	
Rarity:		U
Collector #:	29
Artist:		Dai

Card Name:	Motorcycle Mutant
Civilization:	Darkness
Type:		Creature
Race:		Hedrian
Cost:		4
Power:		6000
Rules Text:	[Blocker]  Blocker (Whenever an opponent's creature attacks, you may tap this creature to stop the attack. Then the 2 creatures battle.)
		This creature can't attack.
		When you put another creature into the battle zone, destroy this creature.
Flavor Text:	
Rarity:		U
Collector #:	31
Artist:		Katsuya

Card Name:	Bruiser Dragon
Civilization:	Fire
Type:		Creature
Race:		Armored Dragon
Cost:		5
Power:		5000
Rules Text:	When this creature is destroyed, choose one of your shields and put it into your graveyard.
Flavor Text:	"It's a Dragon party! Oh yeah! Time to burn this planet to a crisp!"
Rarity:		U
Collector #:	36
Artist:		Asai Genji

Card Name:	Rocketdive Skyterror
Civilization:	Fire
Type:		Creature
Race:		Armored Wyvern
Cost:		4
Power:		5000+
Rules Text:	This creature can't be attacked.
		This creature can't attack players.
		Power attacker +1000 (While attacking, this creature gets +1000 power.)
Flavor Text:	"And just when do all the different kinds of Wyverns that have been asleep in the earth for millions of years wake up? Huh?"
Rarity:		U
Collector #:	41
Artist:		Nottsuo

Card Name:	Volcano Charger
Civilization:	Fire
Type:		Spell
Race:		
Cost:		4
Power:		0
Rules Text:	Destroy one of your opponent's creatures that has power 2000 or less.
		Charger (After you cast this spell, put it into your mana zone instead of your graveyard.)
Flavor Text:	Crimson petals scar the earth, sowing the seeds for the next eruptions.
Rarity:		U
Collector #:	45
Artist:		Mikumo

Card Name:	Coliseum Shell
Civilization:	Nature
Type:		Creature
Race:		Colony Beetle
Cost:		4
Power:		3000
Rules Text:	Whenever this creature becomes blocked, you may put the top card of your deck into your mana zone.
Flavor Text:	"What a terrible, senseless tragedy! My flogoball team was winning!" -Ballom, Master of Death
Rarity:		U
Collector #:	48
Artist:		Katsuya

Card Name:	Dracodance Totem
Civilization:	Nature
Type:		Creature
Race:		Mystery Totem
Cost:		2
Power:		1000
Rules Text:	When this creature would be destroyed, if you have a creature that has Dragon in its race in your mana zone, put this creature into your mana zone instead of destroying it. Then return a creature that has Dragon in its race from your mana zone to your hand.
Flavor Text:	
Rarity:		U
Collector #:	49
Artist:		Norikatsu Miyoshi

Card Name:	Muscle Charger
Civilization:	Nature
Type:		Spell
Race:		
Cost:		3
Power:		0
Rules Text:	Each of your creatures in the battle zone gets +3000 power until the end of the turn.
		Charger (After you cast this spell, put it into your mana zone instead of your graveyard.)
Flavor Text:	"Step 1: Prepare for adventures. Step 2: Have adventures. Wha-hai!!!" -Quixotic Hero Swine Snout
Rarity:		U
Collector #:	50
Artist:		Syuichi Obata

Card Name:	Lunar Charger
Civilization:	Light
Type:		Spell
Race:		
Cost:		3
Power:		0
Rules Text:	Choose up to 2 of your creatures in the battle zone. At the end of the turn, you may untap them.
		Charger (After you cast this spell, put it into your mana zone instead of your graveyard.)
Flavor Text:	Gratho loved bouncing stuff off of walls. That night, the walls had their revenge.
Rarity:		C
Collector #:	8
Artist:		Akira Hamada

Card Name:	Migalo, Vizier of Spycraft
Civilization:	Light
Type:		Creature
Race:		Initiate
Cost:		2
Power:		1500
Rules Text:	Turbo rush (If any of your other creatures broke any shields this turn, this creature gets its [TurboRush]  ability until the end of the turn.)
		[TurboRush] : Whenever this creature attacks, you may look at 2 of your opponent's shields. Then put them back where they were.
Flavor Text:	
Rarity:		C
Collector #:	9
Artist:		Jason

Card Name:	Misha, Channeler of Suns
Civilization:	Light
Type:		Creature
Race:		Mecha del Sol
Cost:		5
Power:		5000
Rules Text:	This creature can't be attacked by any creature that has Dragon in its race.
Flavor Text:	Once the Dragons awoke, so did the Dragon hunters. The Dragon photographers woke up next, followed closely by the Dragon dentists, Dragon insurance agents, and Dragon souvenir-shop owners.
Rarity:		C
Collector #:	10
Artist:		Akira Hamada

Card Name:	Sol Galla, Halo Guardian
Civilization:	Light
Type:		Creature
Race:		Guardian
Cost:		2
Power:		1000+
Rules Text:	[Blocker]  Blocker (Whenever an opponent's creature attacks, you may tap this creature to stop the attack. Then the 2 creatures battle.)
		Whenever a player casts a spell, this creature gets +3000 power until the end of the turn. (Do what the spell says before this creature gets the extra power.)
Flavor Text:	
Rarity:		C
Collector #:	13
Artist:		Iron Pot

Card Name:	Candy Cluster
Civilization:	Water
Type:		Creature
Race:		Cyber Cluster
Cost:		3
Power:		1000
Rules Text:	This creature can't be blocked.
Flavor Text:	Its crunchy outer shell protects a creamy caramel center.
Rarity:		C
Collector #:	17
Artist:		Shishizaru

Card Name:	Eureka Charger
Civilization:	Water
Type:		Spell
Race:		
Cost:		4
Power:		0
Rules Text:	Draw a card.
		Charger (After you cast this spell, put it into your mana zone instead of your graveyard.)
Flavor Text:	"The expression is 'reach for the stars,' not 'reach for the mysterious ball of crackling electricity.'" -Mighty Bandit
Rarity:		C
Collector #:	18
Artist:		Kou1

Card Name:	Grape Globbo
Civilization:	Water
Type:		Creature
Race:		Cyber Virus
Cost:		2
Power:		1000
Rules Text:	When you put this creature into the battle zone, look at your opponent's hand.
Flavor Text:	"I'm quite fully protected from telepathic spies. Indeed, that's what my spiffy hat is for. Wha-hai!!!" -Quixotic Hero Swine Snout
Rarity:		C
Collector #:	19
Artist:		Daisuke Izuka

Card Name:	Illusion Fish
Civilization:	Water
Type:		Creature
Race:		Gel Fish
Cost:		4
Power:		3000
Rules Text:	Turbo rush (If any of your other creatures broke any shields this turn, this creature gets its [TurboRush]  ability until the end of the turn.)
		[TurboRush] : This creature can't be blocked.
Flavor Text:	"Watch closely for the end of history." -Emperor Quazla
Rarity:		C
Collector #:	20
Artist:		Haccan

Card Name:	Corpse Charger
Civilization:	Darkness
Type:		Spell
Race:		
Cost:		4
Power:		0
Rules Text:	Put a creature from your graveyard into your hand.
		Charger (After you cast this spell, put it into your mana zone instead of your graveyard.)
Flavor Text:	The dead rose from their graves and began their slow march into the swamp. Only Argoz wondered who would feed his worms while he was away.
Rarity:		C
Collector #:	26
Artist:		Eiji Kaneda

Card Name:	Gigaclaws
Civilization:	Darkness
Type:		Creature
Race:		Chimera
Cost:		5
Power:		2000
Rules Text:	Turbo rush (If any of your other creatures broke any shields this turn, this creature gets its [TurboRush]  ability until the end of the turn.)
		[TurboRush] : Whenever this creature attacks, your opponent discards his hand.
Flavor Text:	
Rarity:		C
Collector #:	30
Artist:		Hikaru Ikusa

Card Name:	Necrodragon Giland
Civilization:	Darkness
Type:		Creature
Race:		Zombie Dragon
Cost:		4
Power:		6000
Rules Text:	Double breaker (This creature breaks 2 shields.)
		When this creature battles, destroy it after the battle.
Flavor Text:	The rancor and regrets of thousands upon thousands united into one entity-and the cursed Dragon stirred from its slumber.
Rarity:		C
Collector #:	33
Artist:		Miho Midorikawa

Card Name:	Tyrant Worm
Civilization:	Darkness
Type:		Creature
Race:		Parasite Worm
Cost:		1
Power:		2000
Rules Text:	When you put another creature into the battle zone, destroy this creature.
Flavor Text:	It's scared of scared people.
Rarity:		C
Collector #:	35
Artist:		Youichi Kai

Card Name:	Magmadragon Melgars
Civilization:	Fire
Type:		Creature
Race:		Volcano Dragon
Cost:		4
Power:		4000
Rules Text:	
Flavor Text:	Necrodragons slept miles under the swamps. Terradragons hibernated at the root of the great waterfall. Magmadragons slumbered in baths of boiling rock at the volcanoes' cores. One by one, they awoke, arose, and continued their reign of destruction as though millions of years had not passed.
Rarity:		C
Collector #:	39
Artist:		Sansyu

Card Name:	Missile Soldier Ultimo
Civilization:	Fire
Type:		Creature
Race:		Dragonoid
Cost:		3
Power:		2000+
Rules Text:	Turbo rush (If any of your other creatures broke any shields this turn, this creature gets its [TurboRush]  ability until the end of the turn.)
		[TurboRush] : This creature can attack untapped creatures and has "Power attacker +4000 (While attacking, this creature gets +4000 power)."
Flavor Text:	
Rarity:		C
Collector #:	40
Artist:		Syuichi Obata

Card Name:	Torpedo Skyterror
Civilization:	Fire
Type:		Creature
Race:		Armored Wyvern
Cost:		5
Power:		4000+
Rules Text:	While attacking, this creature gets +2000 power for each other tapped creature you have in the battle zone.
Flavor Text:	The Wyverns weren't that excited about the rise of their long-lost cousins-until they saw the chaos and confusion the Dragons caused in the skies.
Rarity:		C
Collector #:	43
Artist:		Naoki Saito

Card Name:	Totto Pipicchi
Civilization:	Fire
Type:		Creature
Race:		Fire Bird
Cost:		3
Power:		1000
Rules Text:	Each creature in the battle zone that has Dragon in its race has "speed attacker." (A creature that has "speed attacker" doesn't get summoning sickness.)
Flavor Text:	The Fire Birds flitted across the sky, their voices raised in joy. The time of awakening had come at last.
Rarity:		C
Collector #:	44
Artist:		Toshiaki Takayama

Card Name:	Bakkra Horn, the Silent
Civilization:	Nature
Type:		Creature
Race:		Horned Beast
Cost:		4
Power:		2000
Rules Text:	Whenever you put a Dragonoid or a creature that has Dragon in its race into the battle zone, put the top card of your deck into your mana zone.
Flavor Text:	Breaking the ancient oath, it led the way to the waterfall where the Terradragons lay dreaming.
Rarity:		C
Collector #:	46
Artist:		Sansyu

Card Name:	Quixotic Hero Swine Snout
Civilization:	Nature
Type:		Creature
Race:		Beast Folk
Cost:		2
Power:		1000+
Rules Text:	Whenever another creature is put into the battle zone, this creature gets +3000 power until the end of the turn.
Flavor Text:	"Make haste, my piggy steed. This pesky rockslide shall not delay us from our afternoon tea with the sultan. Wha-hai!!!"
Rarity:		C
Collector #:	51
Artist:		Yarunoca

Card Name:	Senia, Orchard Avenger
Civilization:	Nature
Type:		Creature
Race:		Tree Folk
Cost:		4
Power:		3000+
Rules Text:	Turbo rush (If any of your other creatures broke any shields this turn, this creature gets its [TurboRush]  ability until the end of the turn.)
		[TurboRush] : This creature gets +5000 power and has "double breaker (This creature breaks 2 shields)."
Flavor Text:	Whole armies have been lost on fruit-salad expeditions.
Rarity:		C
Collector #:	53
Artist:		Toshiaki Takayama

Card Name:	Terradragon Regarion
Civilization:	Nature
Type:		Creature
Race:		Earth Dragon
Cost:		5
Power:		4000+
Rules Text:	Power attacker +3000 (While attacking, this creature gets +3000 power.)
		Double breaker (This creature breaks 2 shields.)
Flavor Text:	Once the Gate of Wards was unwittingly destroyed, there was nothing that could hold the Dragons back.
Rarity:		C
Collector #:	55
Artist:		Daisuke Izuka


Duel Masters, the Duel Masters logo, character names, and their distinctive likenesses are TM and Copyright 1996 Wizards of the Coast/Takara-Tomy/Shogakukan/Mitsui-Kids.