-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 27, 2019 at 01:32 PM
-- Server version: 5.7.23-0ubuntu0.18.04.1
-- PHP Version: 7.2.9-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `duel-masters`
--

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--

CREATE TABLE `cards` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civilization` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `race` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost` smallint(6) NOT NULL,
  `power` int(11) NOT NULL DEFAULT '0',
  `flavor_text` text COLLATE utf8mb4_unicode_ci,
  `rarity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `artist` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cards`
--

INSERT INTO `cards` (`id`, `name`, `image`, `civilization`, `race`, `type`, `cost`, `power`, `flavor_text`, `rarity`, `artist`) VALUES
(1, 'Aqua Sniper', '159a1d9ab5848b9610b1198a444b4825.jpg', 'Water', 'Liquid People', 'Creature', 8, 5000, 'Water surrounds, enfolds, buoys...and attacks.', 'S', 'Seki'),
(2, 'Astrocomet Dragon', 'cca1872581681fdfd6f9c7e9e83766fa.jpg', 'Fire', 'Armored Dragon', 'Creature', 7, 6000, 'Even the smallest weapon attached to it has enough firepower to lay waste to an entire nation.', 'S', 'Yusaku Nakaaki'),
(3, 'Deathblade Beetle', '32506f4c4d1bdf6817c1961830933a7c.jpg', 'Nature', 'Giant Insect', 'Creature', 5, 3000, 'It devours all.', 'S', 'Dustmoss'),
(4, 'Deathliger, Lion of Chaos', '1e9a9dcda9b5cb90dd3b44035f257502.jpg', 'Darkness', 'Demon Command', 'Creature', 7, 9000, 'The world is deafened by the roar of the deathliger.', 'S', 'Atsushi Kawasaki'),
(5, 'Hanusa, Radiance Elemental', 'd72ab7418c2d4b37a04a0d4c363858b0.jpg', 'Light', 'Angel Command', 'Creature', 7, 9500, 'The sky was scorched by Dragons\' fire, and the earth gouged by elementals\' rays. The battle raged for seven nights and seven days.', 'S', 'Masaki Hirooka'),
(6, 'King Depthcon', 'ee75deb46835cb6285daac8619476ef7.jpg', 'Water', 'Leviathan', 'Creature', 7, 6000, 'Leviathans are just like storms. All you can do is wait until they pass.', 'S', 'Ryoya Yuki'),
(7, 'Roaring Great-Horn', 'e1ae100e098e4670789007bcdf1ec56e.jpg', 'Nature', 'Horned Beast', 'Creature', 7, 8000, 'With a mighty, earthshaking roar, the King of Beasts arose.', 'S', 'Ittoku'),
(8, 'Scarlet Skyterror', 'f1862733359b53a15971b7d29d7be515.jpg', 'Fire', 'Armored Wyvern', 'Creature', 8, 3000, 'The forces of Fire will never relent.', 'S', 'Tomofumi Ogasawara'),
(9, 'Urth, Purifying Elemental', '97685921151f26a1db16361ce3c5ad19.jpg', 'Light', 'Angel Command', 'Creature', 6, 6000, 'It is said the destruction of the Ancient Empire was caused by the descent of the elementals.', 'S', 'Akifumi Yamamoto'),
(10, 'Zagaan, Knight of Darkness', 'd485b139dcbd52f8a4fa56897484a110.jpg', 'Darkness', 'Demon Command', 'Creature', 6, 7000, '\"If I were to send him to the field, he would overwhelm the enemy in an instant. Where is the fun in that?\" -Ballom, Master of Death', 'S', 'Tsutomu Kawade'),
(11, 'Gran Gure, Space Guardian', NULL, 'Light', 'Guardian', 'Creature', 6, 9000, 'Those who defile the Sky Castle are doomed, for they shall know the wrath of the Guardians.', 'V', 'D-Suzuki'),
(12, 'Rayla, Truth Enforcer', NULL, 'Light', 'Berserker', 'Creature', 6, 3000, 'The Enforcers deliver the oracles\' words, which bear the power of truth.', 'V', 'Dai'),
(13, 'King Ripped-Hide', NULL, 'Water', 'Leviathan', 'Creature', 7, 5000, 'Long ago, the Cyber Lords built a city on his back.', 'V', 'Miho Midorikawa'),
(14, 'Seamine', NULL, 'Water', 'Fish', 'Creature', 6, 4000, 'If the last thing in your life you want to see is something beautiful, attack an underwater city.', 'V', 'Tomofumi Ogasawara'),
(15, 'Gigargon', NULL, 'Darkness', 'Chimera', 'Creature', 8, 3000, 'There is no rest in the Dark Realm, even after death.', 'V', 'Tsutomu Kawade'),
(16, 'Vampire Silphy', NULL, 'Darkness', 'Dark Lord', 'Creature', 8, 4000, 'Dark Lords have no mercy for the weak.', 'V', 'Masaki Hirooka'),
(17, 'Bolshack Dragon', NULL, 'Fire', 'Armored Dragon', 'Creature', 6, 6000, 'The last city that offended it is now a ruin.', 'V', 'Ryoya Yuki'),
(18, 'Gatling Skyterror', NULL, 'Fire', 'Armored Wyvern', 'Creature', 7, 7000, 'Part chopper, part chomper.', 'V', 'Hisashi Momose'),
(19, 'Thorny Mandra', NULL, 'Nature', 'Tree Folk', 'Creature', 5, 4000, 'A beautiful flower, an alluring scent...and deadly thorns.', 'V', 'Atsushi Kawasaki'),
(20, 'Tower Shell', NULL, 'Nature', 'Colony Beetle', 'Creature', 6, 5000, 'Not many things want to get in its way.', 'V', 'Atsushi Kawasaki'),
(21, 'Chilias, the Oracle', NULL, 'Light', 'Light Bringer', 'Creature', 4, 2500, 'The shining towers in the sky exist to protect the oracles.', 'R', 'Akira Hamada'),
(22, 'Dia Nork, Moonlight Guardian', NULL, 'Light', 'Guardian', 'Creature', 4, 5000, 'Flying is the greatest taboo of any race that has encountered the Guardians.', 'R', 'Tomofumi Ogasawara'),
(23, 'Holy Awe', NULL, 'Light', '', 'Spell', 6, 0, '', 'R', 'Naoki Saito'),
(24, 'Lah, Purification Enforcer', NULL, 'Light', 'Berserker', 'Creature', 5, 5500, 'Those who have seen this Enforcer believe that eclipses are omens of disaster.', 'R', 'Hideaki Takamura'),
(25, 'Laser Wing', NULL, 'Light', '', 'Spell', 5, 0, 'This is light speed.', 'R', 'Gyokan'),
(26, 'Szubs Kin, Twilight Guardian', NULL, 'Light', 'Guardian', 'Creature', 5, 6000, 'Those who dwell in the sky towers eradicate all intruders.', 'R', 'Ittoku'),
(27, 'Aqua Knight', NULL, 'Water', 'Liquid People', 'Creature', 5, 4000, 'Like a whirlpool, it never seems to end.', 'R', 'D-Suzuki'),
(28, 'Crystal Memory', NULL, 'Water', '', 'Spell', 4, 0, '', 'R', 'Ryoya Yuki'),
(29, 'Saucer-Head Shark', NULL, 'Water', 'Gel Fish', 'Creature', 5, 3000, '\"Isn\'t it cool? The heads can be combined.\" -Tropico', 'R', 'Naoki Saito'),
(30, 'Teleportation', NULL, 'Water', '', 'Spell', 5, 0, '\"Start all over from your previous life!\" -Crystal Lancer', 'R', 'Akifumi Yamamoto'),
(31, 'Tropico', NULL, 'Water', 'Cyber Lord', 'Creature', 5, 3000, '\"I will lead you into battle.\"', 'R', 'Jason'),
(32, 'Unicorn Fish', NULL, 'Water', 'Fish', 'Creature', 4, 1000, 'It attacks anything that enters its territory, even Leviathans.', 'R', 'Hisanobu Kometani'),
(33, 'Creeping Plague', NULL, 'Darkness', '', 'Spell', 1, 0, 'Witness the power of the Dark!', 'R', 'Ryoya Yuki'),
(34, 'Dark Clown', NULL, 'Darkness', 'Brain Jacker', 'Creature', 4, 6000, '', 'R', 'Jason'),
(35, 'Gigaberos', NULL, 'Darkness', 'Chimera', 'Creature', 5, 8000, '', 'R', 'Katsuya'),
(36, 'Gigagiele', NULL, 'Darkness', 'Chimera', 'Creature', 5, 3000, 'By assembling parts of different magical beasts, the Dark Lords created a nightmare.', 'R', 'Hisashi Momose'),
(37, 'Night Master, Shadow of Decay', NULL, 'Darkness', 'Ghost', 'Creature', 6, 3000, 'Don\'t turn around. You wouldn\'t want to see what\'s behind you.', 'R', 'Hideaki Takamura'),
(38, 'Terror Pit', NULL, 'Darkness', '', 'Spell', 6, 0, '', 'R', 'Yusaku Nakaaki'),
(39, 'Chaos Strike', NULL, 'Fire', '', 'Spell', 2, 0, '\"You won\'t escape me!\"', 'R', 'Soushi Hirose'),
(40, 'Draglide', NULL, 'Fire', 'Armored Wyvern', 'Creature', 5, 5000, 'Draglides grow with their mounts. Their bond is their weapon.', 'R', 'Ittoku'),
(41, 'Explosive Fighter Ucarn', NULL, 'Fire', 'Dragonoid', 'Creature', 5, 9000, 'Dragonoid fighters never retreat. They believe that a heroic fighter will become a Dragon in his next life.', 'R', 'Hisanobu Kometani'),
(42, 'Magma Gazer', NULL, 'Fire', '', 'Spell', 3, 0, '', 'R', 'Masaki Hirooka'),
(43, 'Nomad Hero Gigio', NULL, 'Fire', 'Machine Eater', 'Creature', 5, 3000, '\"Time to move on. So many unexplored worlds; so much trash to find!\" -Nomad Hero Gigio', 'R', 'Eiji Kaneda'),
(44, 'Rothus, the Traveler', NULL, 'Fire', 'Armorloid', 'Creature', 4, 4000, '\"Though battered and scarred, I march on through wars, storms, and strife. I march on.\"', 'R', 'Seki'),
(45, 'Aura Blast', NULL, 'Nature', '', 'Spell', 4, 0, 'Hear the roar of the mighty Earth!', 'R', 'Nottsuo'),
(46, 'Natural Snare', NULL, 'Nature', '', 'Spell', 6, 0, '', 'R', 'Sansyu'),
(47, 'Red-Eye Scorpion', NULL, 'Nature', 'Giant Insect', 'Creature', 5, 4000, 'Never start a fire in the depths of a forest. Enraged red-eye scorpions will come after you.', 'R', 'Kou1'),
(48, 'Stampeding Longhorn', NULL, 'Nature', 'Horned Beast', 'Creature', 5, 4000, 'A stampeding longhorn leaves nothing but dust in its wake.', 'R', 'Norikatsu Miyoshi'),
(49, 'Storm Shell', NULL, 'Nature', 'Colony Beetle', 'Creature', 7, 2000, 'A Colony Beetle\'s spawning is a virtual nightmare. It covers the earth with a hail of its bullet-like eggs.', 'R', 'Sansyu'),
(50, 'Tri-horn Shepherd', NULL, 'Nature', 'Horned Beast', 'Creature', 5, 5000, 'Sometimes weeks pass before it realizes it has a gored enemy stuck to its horns.', 'R', 'Daisuke Izuka'),
(51, 'Frei, Vizier of Air', NULL, 'Light', 'Initiate', 'Creature', 4, 3000, '\"Vizier of Air, arise. Your fingers are sacred swords.\" -Hanusa, Radiance Elemental', 'U', 'Yusaku Nakaaki'),
(52, 'Iocant, the Oracle', NULL, 'Light', 'Light Bringer', 'Creature', 2, 2000, '', 'U', 'Naoki Saito'),
(53, 'Lok, Vizier of Hunting', NULL, 'Light', 'Initiate', 'Creature', 4, 4000, '\"Vizier of Hunting, arise. Your words are arrows of light.\" -Hanusa, Radiance Elemental', 'U', 'Hisanobu Kometani'),
(54, 'Moonlight Flash', NULL, 'Light', '', 'Spell', 4, 0, 'Cower before the purity of the Angel Commands!', 'U', 'Hisanobu Kometani'),
(55, 'Ruby Grass', NULL, 'Light', 'StarLight Tree', 'Creature', 3, 3000, '', 'U', 'Naoki Saito'),
(56, 'Toel, Vizier of Hope', NULL, 'Light', 'Initiate', 'Creature', 5, 2000, '\"Vizier of Hope, arise. Your singing voice is a breeze of dawn.\" -Hanusa, Radiance Elemental', 'U', 'Masaki Hirooka'),
(57, 'Aqua Soldier', NULL, 'Water', 'Liquid People', 'Creature', 3, 1000, '\"What a spirit for such a skinny little thing.\" -Brawler Zyler', 'U', 'Jason'),
(58, 'Brain Serum', NULL, 'Water', '', 'Spell', 4, 0, '', 'U', 'Dreamwave'),
(59, 'Faerie Child', NULL, 'Water', 'Cyber Virus', 'Creature', 4, 2000, 'A school of teeny, tiny points of light. Stopping them all is impossible.', 'U', 'Hisanobu Kometani'),
(60, 'Illusionary Merfolk', NULL, 'Water', 'Gel Fish', 'Creature', 5, 4000, 'The Cyber Lords\' devotion to beauty is so extreme, they even designed their warrior race to have a pleasant appearance.', 'U', 'Kou1'),
(61, 'King Coral', NULL, 'Water', 'Leviathan', 'Creature', 3, 1000, 'Leviathans mature slowly, eventually growing to become the largest creatures in the world.', 'U', 'Hideaki Takamura'),
(62, 'Revolver Fish', NULL, 'Water', 'Gel Fish', 'Creature', 4, 5000, '', 'U', 'Naoki Saito'),
(63, 'Bone Spider', NULL, 'Darkness', 'Living Dead', 'Creature', 3, 5000, 'They can wait forever, for they know they will not die alone.', 'U', 'Dai'),
(64, 'Dark Raven, Shadow of Grief', NULL, 'Darkness', 'Ghost', 'Creature', 4, 1000, 'If you\'ve ever thought there was something hiding in the dark, you were right.', 'U', 'Norikatsu Miyoshi'),
(65, 'Dark Reversal', NULL, 'Darkness', '', 'Spell', 2, 0, '', 'U', 'Nottsuo'),
(66, 'Masked Horror, Shadow of Scorn', NULL, 'Darkness', 'Ghost', 'Creature', 5, 1000, 'Seeking to live forever, they left their bodies behind.', 'U', 'Gyokan'),
(67, 'Stinger Worm', NULL, 'Darkness', 'Parasite Worm', 'Creature', 3, 5000, 'When worms kill a creature, they can still make it move.', 'U', 'Norikatsu Miyoshi'),
(68, 'Swamp Worm', NULL, 'Darkness', 'Parasite Worm', 'Creature', 7, 2000, 'Flesh is its food. Bones are its toothpicks.', 'U', 'Youichi Kai'),
(69, 'Armored Walker Urherion', NULL, 'Fire', 'Armorloid', 'Creature', 4, 3000, '\"The world at peace? I\'d rather see the world in pieces.\"', 'U', 'Tsutomu Kawade'),
(70, 'Meteosaur', NULL, 'Fire', 'Rock Beast', 'Creature', 5, 2000, '', 'U', 'Masateru Ikeda'),
(71, 'Onslaughter Triceps', NULL, 'Fire', 'Dragonoid', 'Creature', 3, 5000, 'The first to the front lines are the first to taste victory.', 'U', 'Katsuya'),
(72, 'Stonesaur', NULL, 'Fire', 'Rock Beast', 'Creature', 5, 4000, 'There is no sanctuary in the Fire Territory. Especially on the featureless, rocky plains.', 'U', 'Miho Midorikawa'),
(73, 'Super Explosive Volcanodon', NULL, 'Fire', 'Dragonoid', 'Creature', 4, 2000, 'After it attacks, the lucky are left with rubble.', 'U', 'Naoki Saito'),
(74, 'Tornado Flame', NULL, 'Fire', '', 'Spell', 5, 0, '', 'U', 'Soushi Hirose'),
(75, 'Coiling Vines', NULL, 'Nature', 'Tree Folk', 'Creature', 4, 3000, 'They eat vegetarians.', 'U', 'D-Suzuki'),
(76, 'Dome Shell', NULL, 'Nature', 'Colony Beetle', 'Creature', 4, 3000, 'Colony Beetles feed on the World Tree to build gigantic nests on their backs.', 'U', 'Hisashi Momose'),
(77, 'Forest Hornet', NULL, 'Nature', 'Giant Insect', 'Creature', 4, 4000, 'With its deadly sting and crushing jaws, it makes the Horned Beasts cower.', 'U', 'Youichi Kai'),
(78, 'Pangaea\'s Song', NULL, 'Nature', '', 'Spell', 1, 0, '\"Here\'s something better for you to do.\" -Fighter Dual Fang', 'U', 'Seki'),
(79, 'Poisonous Dahlia', NULL, 'Nature', 'Tree Folk', 'Creature', 4, 5000, '\"I will never give you up.\" -Symbolic meaning of the poisonous dahlia in flower language', 'U', 'Hideaki Takamura'),
(80, 'Poisonous Mushroom', NULL, 'Nature', 'Balloon Mushroom', 'Creature', 2, 1000, 'You won\'t find these mushrooms on salads.', 'U', 'Daisuke Izuka'),
(81, 'Emerald Grass', NULL, 'Light', 'StarLight Tree', 'Creature', 2, 3000, '', 'C', 'Daisuke Izuka'),
(82, 'Iere, Vizier of Bullets', NULL, 'Light', 'Initiate', 'Creature', 3, 3000, '\"Vizier of Bullets, arise. Your will is an arrow that never misses the target.\" -Hanusa, Radiance Elemental', 'C', 'Akira Hamada'),
(83, 'La Ura Giga, Sky Guardian', NULL, 'Light', 'Guardian', 'Creature', 1, 2000, 'To protect their floating cities, Guardians are gifted with shining wings.', 'C', 'Kou1'),
(84, 'Miele, Vizier of Lightning', NULL, 'Light', 'Initiate', 'Creature', 3, 1000, '\"Vizier of Lightning, arise. Your eyes are a bow of judgment.\" -Hanusa, Radiance Elemental', 'C', 'Dai'),
(85, 'Reusol, the Oracle', NULL, 'Light', 'Light Bringer', 'Creature', 2, 2000, 'Soul and light enmeshed in a web of glory.', 'C', 'Soushi Hirose'),
(86, 'Senatine Jade Tree', NULL, 'Light', 'StarLight Tree', 'Creature', 3, 4000, 'Floating trees in the sky generate beautiful light from poisoned air.', 'C', 'Norikatsu Miyoshi'),
(87, 'Solar Ray', NULL, 'Light', '', 'Spell', 2, 0, '', 'C', 'Jason'),
(88, 'Sonic Wing', NULL, 'Light', '', 'Spell', 3, 0, '\"Can\'t catch me!\"', 'C', 'Masaki Hirooka'),
(89, 'Aqua Hulcus', NULL, 'Water', 'Liquid People', 'Creature', 3, 2000, 'Liquid People freely control liquid. For them, water is armor and shield.', 'C', 'Eiji Kaneda'),
(90, 'Aqua Vehicle', NULL, 'Water', 'Liquid People', 'Creature', 2, 1000, 'The chip implanted in its head lets the Cyber Lords control its brain waves.', 'C', 'Atsushi Kawasaki'),
(91, 'Candy Drop', NULL, 'Water', 'Cyber Virus', 'Creature', 3, 1000, 'No mouth. Big bite.', 'C', 'Gyokan'),
(92, 'Hunter Fish', NULL, 'Water', 'Fish', 'Creature', 2, 3000, 'It lies in wait, hidden in coral reefs, lashing out ferociously when it senses prey.', 'C', 'Masateru Ikeda'),
(93, 'Marine Flower', NULL, 'Water', 'Cyber Virus', 'Creature', 1, 2000, 'It blooms in the ocean depths, far from any light.', 'C', 'Hikaru Ikusa'),
(94, 'Phantom Fish', NULL, 'Water', 'Gel Fish', 'Creature', 3, 4000, '', 'C', 'Ittoku'),
(95, 'Spiral Gate', NULL, 'Water', '', 'Spell', 2, 0, '', 'C', 'Tomofumi Ogasawara'),
(96, 'Virtual Tripwire', NULL, 'Water', '', 'Spell', 3, 0, 'It will stay until it dies.', 'C', 'Akifumi Yamamoto'),
(97, 'Black Feather, Shadow of Rage', NULL, 'Darkness', 'Ghost', 'Creature', 1, 3000, '', 'C', 'Soushi Hirose'),
(98, 'Bloody Squito', NULL, 'Darkness', 'Brain Jacker', 'Creature', 2, 4000, '', 'C', 'Atsushi Kawasaki'),
(99, 'Bone Assassin, the Ripper', NULL, 'Darkness', 'Living Dead', 'Creature', 4, 2000, 'Commanders send them to the front line. It\'s too dangerous to have them close.', 'C', 'Nottsuo'),
(100, 'Death Smoke', NULL, 'Darkness', '', 'Spell', 4, 0, 'Savor the taste of fear!', 'C', 'Dustmoss'),
(101, 'Ghost Touch', NULL, 'Darkness', '', 'Spell', 2, 0, '', 'C', 'Soushi Hirose'),
(102, 'Skeleton Soldier, the Defiled', NULL, 'Darkness', 'Living Dead', 'Creature', 4, 3000, '\"Rip. Tear. Gnash. They all make the same delightful noises.\"', 'C', 'Katsuya'),
(103, 'Wandering Braineater', NULL, 'Darkness', 'Living Dead', 'Creature', 2, 2000, 'Devouring nice, fresh brains is all they know . . . and all they care about.', 'C', 'Youichi Kai'),
(104, 'Writhing Bone Ghoul', NULL, 'Darkness', 'Living Dead', 'Creature', 2, 2000, 'Seeking power, they gave up their intelligence. What they found was pain.', 'C', 'Eiji Kaneda'),
(105, 'Artisan Picora', NULL, 'Fire', 'Machine Eater', 'Creature', 1, 2000, '\"Fix it? That\'s easy. Just hit it.\" -Legendary Artisan Picora', 'C', 'Akifumi Yamamoto'),
(106, 'Brawler Zyler', NULL, 'Fire', 'Humanoid', 'Creature', 2, 1000, '\"I wouldn\'t, if I were you. You don\'t want to get hurt.\"', 'C', 'Tsutomu Kawade'),
(107, 'Burning Power', NULL, 'Fire', '', 'Spell', 1, 0, 'Witness the power of whirling fire!', 'C', 'Miho Midorikawa'),
(108, 'Crimson Hammer', NULL, 'Fire', '', 'Spell', 2, 0, 'There\'s nothing a Machine Eater enjoys more than a good game of Whack-a-Hulcus.', 'C', 'Daisuke Izuka'),
(109, 'Deadly Fighter Braid Claw', NULL, 'Fire', 'Dragonoid', 'Creature', 1, 1000, 'A Dragonoid\'s true home is the battlefield.', 'C', 'Yusaku Nakaaki'),
(110, 'Fatal Attacker Horvath', NULL, 'Fire', 'Humanoid', 'Creature', 3, 2000, '\"Any final words?\"', 'C', 'Dai'),
(111, 'Fire Sweeper Burning Hellion', NULL, 'Fire', 'Dragonoid', 'Creature', 4, 3000, 'Those who leave battle last are the ones who savor victory the longest.', 'C', 'Sansyu'),
(112, 'Immortal Baron, Vorg', NULL, 'Fire', 'Humanoid', 'Creature', 2, 2000, '\"You see? The one who wins is the one who has the bigger weapon.\"', 'C', 'Dustmoss'),
(113, 'Bronze-Arm Tribe', NULL, 'Nature', 'Beast Folk', 'Creature', 3, 1000, 'They\'re bringing some friends to the party.', 'C', 'Yusaku Nakaaki'),
(114, 'Burning Mane', NULL, 'Nature', 'Beast Folk', 'Creature', 2, 2000, 'It pulled a young tree up by the roots, swinging it over its head as though it were a twig.', 'C', 'Sansyu'),
(115, 'Dimension Gate', NULL, 'Nature', '', 'Spell', 3, 0, '', 'C', 'Jason'),
(116, 'Fear Fang', NULL, 'Nature', 'Beast Folk', 'Creature', 3, 3000, 'The Beast Folk are gifted with huge muscles to withstand the tremendous gravity of the World Tree.', 'C', 'Norikatsu Miyoshi'),
(117, 'Golden Wing Striker', NULL, 'Nature', 'Beast Folk', 'Creature', 3, 2000, 'Hawk warriors hunt faster than the wind.', 'C', 'Miho Midorikawa'),
(118, 'Mighty Shouter', NULL, 'Nature', 'Beast Folk', 'Creature', 3, 2000, '\"I am not pink!\"', 'C', 'Dustmoss'),
(119, 'Steel Smasher', NULL, 'Nature', 'Beast Folk', 'Creature', 2, 3000, '\"Me big. Me tough. Me smash.\" -Iron Hammer, Boar Warrior', 'C', 'Katsuya'),
(120, 'Ultimate Force', NULL, 'Nature', '', 'Spell', 5, 0, '\"Great Earth, give me your might!\" -Fighter Dual Fang', 'C', 'Sansyu');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(7, '2019_01_26_192912_create_cards_table', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
